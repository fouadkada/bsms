/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.ListRenderers;

import com.fouadkada.bsms.views.resource_management.ResourceManagement;
import java.awt.Color;
import java.awt.Component;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.border.Border;

/**
 *
 * @author fouad
 */
public class ColoredCellRenderer extends DefaultListCellRenderer {

    private Border _unselectedBorder = null;
    private Border _selectedBorder = null;
    private boolean _isBordered = true;

    @Override
    public void setBackground(Color col) {
        
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object color,
            int index, boolean isSelected, boolean cellHasFocus) {
        try {
            setText(" ");
            Field f = Class.forName("java.awt.Color").getField(String.valueOf(color));
            super.setBackground((Color) f.get(null));
            if (_isBordered) {
                if (isSelected) {
                    if (_selectedBorder == null) {
                        _selectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5, list.getSelectionBackground());
                    }
                    setBorder(_selectedBorder);
                } else {
                    if (_unselectedBorder == null) {
                        _unselectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5, list.getBackground());
                    }
                    setBorder(_unselectedBorder);
                }
            }
            return this;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ResourceManagement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchFieldException ex) {
            Logger.getLogger(ResourceManagement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(ResourceManagement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(ResourceManagement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ResourceManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
