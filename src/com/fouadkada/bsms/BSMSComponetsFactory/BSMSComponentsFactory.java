/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.BSMSComponetsFactory;

import com.fouadkada.bsms.BSMSCustomComponents.BSMSAppointmentComponent;
import com.fouadkada.bsms.BSMSCustomComponents.BSMSResourceComponent;
import com.fouadkada.bsms.Listeners.BSMSAppointmentListener;
import com.fouadkada.bsms.models.BSMSAppointment;
import com.fouadkada.bsms.models.BSMSResource;
import com.thirdnf.resourceScheduler.Appointment;
import com.thirdnf.resourceScheduler.Resource;
import com.thirdnf.resourceScheduler.components.AbstractAppointmentComponent;
import com.thirdnf.resourceScheduler.components.AbstractResourceComponent;
import com.thirdnf.resourceScheduler.components.BasicComponentFactory;

/**
 *
 * @author fouad
 */
public class BSMSComponentsFactory extends BasicComponentFactory {

    private BSMSAppointmentListener appointmentListener = null;
    private BSMSAppointmentComponent component;
    private BSMSResourceComponent resource;

    @Override
    public AbstractAppointmentComponent makeAppointmentComponent(Appointment appointment) {
        component = new BSMSAppointmentComponent((BSMSAppointment) appointment);
        component.setAppointmentListener(appointmentListener);
        return component;
    }

    public void setAppointmentListener(BSMSAppointmentListener appointmentListener) {
        this.appointmentListener = appointmentListener;
    }

    @Override
    public AbstractResourceComponent makeResourceComponent(Resource resource) {
        this.resource = new BSMSResourceComponent((BSMSResource) resource);
        return this.resource;
    }
}
