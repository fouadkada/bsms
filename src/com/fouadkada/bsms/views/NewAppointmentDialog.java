package com.fouadkada.bsms.views;

import com.fouadkada.bsms.controllers.BSMSAppointmentTools;
import com.fouadkada.bsms.controllers.DataBundle;
import com.fouadkada.bsms.models.BSMSAppointment;
import com.fouadkada.bsms.models.BSMSClient;
import com.fouadkada.bsms.models.BSMSResource;
import com.fouadkada.bsms.models.BSMSService;
import com.fouadkada.bsms.models.BSMSSettings;
import com.fouadkada.bsms.models.SwingModels.ComboBoxModels.BSMSClientsComboBoxModel;
import com.fouadkada.bsms.models.SwingModels.ComboBoxModels.BSMSResourcesComboBoxModel;
import com.fouadkada.bsms.models.SwingModels.ListModels.BSMSServicesListModel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import org.joda.time.Duration;
import org.joda.time.LocalDateTime;

/**
 *
 * @author fouad
 */
public class NewAppointmentDialog extends javax.swing.JFrame {

    private BSMSResourcesComboBoxModel resourcesModel;
    private BSMSClientsComboBoxModel clientsModel;
    private BSMSServicesListModel servicesModel;
    private Main parentFrame;
    private boolean editingMode;
    private DataBundle bundle;
    private BSMSSettings settings;
    private Calendar startingHoursCalendar;
    private Calendar endingHoursCalendar;
    private Calendar mainViewCalendar;
    private final String CREATE_CONFIRMATION = "Are you sure you want to create a new appointment?";
    private final String APPOINTMENT_TIME_NOT_WITHIN_WORKING_HOURS = "Appointment's time is not in the workig hours.";
    private final String FIELDS_REQUIRED = "You did not enter all required fields.";
    private final String CONFIRM_MESSAGE = "Confirm";
    private final String ERROR_MESSAGE = "Error";

    /**
     * Creates new form NewAppointmentDialog
     */
    public NewAppointmentDialog(boolean editingMode,
            DataBundle bundle,
            BSMSSettings settings,
            Calendar mainViewCalendar) {
        this.editingMode = editingMode;
        this.bundle = bundle;
        this.settings = settings;
        this.mainViewCalendar = mainViewCalendar;

        resourcesModel = new BSMSResourcesComboBoxModel();
        clientsModel = new BSMSClientsComboBoxModel();
        servicesModel = new BSMSServicesListModel();

        initComponents();

        /**
         * this will be used to set the spinner to the starting working hours
         */
        startingHoursCalendar = Calendar.getInstance();
        startingHoursCalendar.setTime(settings.getStartTime());

        int hoursToSet = startingHoursCalendar.get(Calendar.HOUR_OF_DAY);
        if (hoursToSet == 0) {
            timeHoursSpinner.setValue((int) (hoursToSet + 12));
            amRadioButton.setSelected(true);
            pmRadioButton.setSelected(false);
        } else if (hoursToSet > 12) {
            timeHoursSpinner.setValue((int) (hoursToSet - 12));
            amRadioButton.setSelected(false);
            pmRadioButton.setSelected(true);
        } else if (hoursToSet < 12) {
            timeHoursSpinner.setValue((hoursToSet));
            amRadioButton.setSelected(true);
            pmRadioButton.setSelected(false);
        } else if (hoursToSet == 12) {
            timeHoursSpinner.setValue((int) (hoursToSet));
            amRadioButton.setSelected(false);
            pmRadioButton.setSelected(true);
        }
        timeMinutesSpinner.setValue((int) startingHoursCalendar.get(Calendar.MINUTE));

        if (this.editingMode && this.bundle != null) {
            ArrayList<Integer> tempIndices = new ArrayList<>();
            resourcesModel.setSelectedItem(this.bundle.getSelectedResource());
            clientsModel.setSelectedItem(this.bundle.getSelectedClient());
            for (int i = 0; i < bundle.getSelectedServices().size(); i++) {
                for (int j = 0; j < servicesModel.size(); j++) {
                    if (this.bundle.getSelectedServices().get(i).equals(servicesModel.elementAt(j))) {
                        tempIndices.add(j);
                    }
                }
            }
            int[] indices = new int[tempIndices.size()];
            for (int i = 0; i < tempIndices.size(); i++) {
                indices[i] = tempIndices.get(i);
            }
            servicesList.setSelectedIndices(indices);

            dateChooser.setDate(this.bundle.getSelectedDate().toDate());
            timeHoursSpinner.setValue(this.bundle.getSelectedHour());
            timeMinutesSpinner.setValue(this.bundle.getSelectedMinute());
            durationHoursSpinner.setValue(this.bundle.getDurationHours());
            durationMinutesSpinner.setValue(this.bundle.getDurationMinutes());
            if (this.bundle.isAmSelected()) {
                amRadioButton.setSelected(true);
                pmRadioButton.setSelected(false);
            } else {
                pmRadioButton.setSelected(true);
                amRadioButton.setSelected(false);
            }

            createButton.setText("update");
        }
    }

    protected void setParentFrame(Main parentFrame) {
        this.parentFrame = parentFrame;
    }

    protected void setSettings(BSMSSettings settings) {
        this.settings = settings;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        employeeCombo = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        clientCombo = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        servicesList = new javax.swing.JList();
        jLabel7 = new javax.swing.JLabel();
        dateChooser = new com.toedter.calendar.JDateChooser();
        jLabel8 = new javax.swing.JLabel();
        timeHoursSpinner = new javax.swing.JSpinner();
        timeMinutesSpinner = new javax.swing.JSpinner();
        amRadioButton = new javax.swing.JRadioButton();
        pmRadioButton = new javax.swing.JRadioButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        durationHoursSpinner = new javax.swing.JSpinner();
        jLabel11 = new javax.swing.JLabel();
        durationMinutesSpinner = new javax.swing.JSpinner();
        resetButton = new javax.swing.JButton();
        createButton = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("New Appointment");
        setAlwaysOnTop(true);
        setResizable(false);

        jLabel1.setText("Employee:");

        employeeCombo.setModel(resourcesModel      );

        jLabel2.setText("Client:");

        clientCombo.setModel(clientsModel);

        jLabel3.setText("Services:");

        servicesList.setModel(servicesModel);
        servicesList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                servicesListMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(servicesList);

        jLabel7.setText("Date:");

        dateChooser.setCalendar(Calendar.getInstance());

        jLabel8.setText("Time:");

        timeHoursSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 1, 12, 1));

        timeMinutesSpinner.setModel(new javax.swing.SpinnerNumberModel(0, 0, 59, 1));

        buttonGroup1.add(amRadioButton);
        amRadioButton.setSelected(true);
        amRadioButton.setText("AM");

        buttonGroup1.add(pmRadioButton);
        pmRadioButton.setText("PM");

        jLabel9.setText("Duration:");

        jLabel10.setText(":");

        durationHoursSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 0, 12, 1));

        jLabel11.setText("hours");

        durationMinutesSpinner.setModel(new javax.swing.SpinnerNumberModel(0, 0, 59, 1));

        resetButton.setText("reset");
        resetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetButtonActionPerformed(evt);
            }
        });

        createButton.setText("create");
        createButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createButtonActionPerformed(evt);
            }
        });

        jLabel12.setText("minutes");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(createButton)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(resetButton))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel1)
                                .addComponent(jLabel2)
                                .addComponent(jLabel3)
                                .addComponent(jLabel9))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(durationHoursSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel11)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(durationMinutesSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel12))
                                .addComponent(clientCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(employeeCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(dateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(timeHoursSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(3, 3, 3)
                                    .addComponent(timeMinutesSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(amRadioButton)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(pmRadioButton))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(employeeCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(clientCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(dateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(timeHoursSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(amRadioButton)
                    .addComponent(pmRadioButton)
                    .addComponent(jLabel10)
                    .addComponent(timeMinutesSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(durationHoursSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(durationMinutesSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(resetButton)
                    .addComponent(createButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void createButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createButtonActionPerformed
        if (!this.editingMode) {
            // we are in new appoitment mode
            if (employeeCombo.getSelectedIndex() >= 0
                    && clientCombo.getSelectedIndex() >= 0
                    && servicesList.getSelectedIndex() >= 0
                    && dateChooser.getDate().getTime() >= 0) {
                int choice = JOptionPane.showConfirmDialog(this,
                        CREATE_CONFIRMATION,
                        CONFIRM_MESSAGE,
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.INFORMATION_MESSAGE);
                if (choice == JOptionPane.YES_OPTION) {
                    startingHoursCalendar = Calendar.getInstance();
                    startingHoursCalendar.setTime(settings.getStartTime());

                    endingHoursCalendar = Calendar.getInstance();
                    endingHoursCalendar.setTime(settings.getEndTime());

                    int settingsStartHours = startingHoursCalendar.get(Calendar.HOUR_OF_DAY);
                    int settingsEndHours = endingHoursCalendar.get(Calendar.HOUR_OF_DAY);

                    int appTime = (int) timeHoursSpinner.getValue();
                    if (pmRadioButton.isSelected()) {
                        if (appTime != 12) {
                            appTime += 12;
                        }
                    }

                    if (!(appTime < settingsStartHours)
                            && !(appTime >= settingsEndHours)) {
                        BSMSAppointment newAppointment = new BSMSAppointment();
                        newAppointment.setResource((BSMSResource) employeeCombo.getSelectedItem());
                        newAppointment.setClient((BSMSClient) clientCombo.getSelectedItem());
                        newAppointment.setStatus("not paid");

                        Calendar calendar = dateChooser.getCalendar();
                        int hour = 0;
                        if (amRadioButton.isSelected()) {
                            hour = (int) timeHoursSpinner.getValue();
                        } else if (pmRadioButton.isSelected()) {
                            hour = (int) timeHoursSpinner.getValue() + 12;
                        }

                        calendar.set(Calendar.HOUR_OF_DAY, hour);
                        calendar.set(Calendar.MINUTE, (int) timeMinutesSpinner.getValue());

                        Date currentDate = new Date(calendar.getTimeInMillis());

                        calendar.add(Calendar.HOUR, (int) durationHoursSpinner.getValue());
                        calendar.add(Calendar.MINUTE, (int) durationMinutesSpinner.getValue());
                        Date dateAfterDuration = new Date(calendar.getTimeInMillis());

                        newAppointment.setDateTime(new LocalDateTime(currentDate.getTime()));
                        newAppointment.setDuration(new Duration(currentDate.getTime(), dateAfterDuration.getTime()));

                        List<BSMSService> services = new ArrayList<>();
                        double amount = 0;
                        for (int i = 0; i < servicesList.getSelectedValuesList().size(); i++) {
                            services.add((BSMSService) servicesList.getSelectedValuesList().get(i));
                            amount += ((BSMSService) servicesList.getSelectedValuesList().get(i)).getPrice();
                        }
                        newAppointment.setServices(services);
                        newAppointment.setAmount(amount);
                        long newAppointmentId = BSMSAppointmentTools.addAppointment(newAppointment);
                        if ((currentDate.getYear() + 1900) == mainViewCalendar.get(Calendar.YEAR)
                                && currentDate.getDate() == mainViewCalendar.get(Calendar.DAY_OF_MONTH)
                                && currentDate.getMonth() == mainViewCalendar.get(Calendar.MONTH)) {
                            newAppointment.setAid(newAppointmentId);
                            parentFrame.addNewAppointment(newAppointment);
                        }
                        this.dispose();
                    } else {
                        JOptionPane.showMessageDialog(this,
                                APPOINTMENT_TIME_NOT_WITHIN_WORKING_HOURS,
                                ERROR_MESSAGE,
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            } else {
                JOptionPane.showMessageDialog(this,
                        FIELDS_REQUIRED,
                        ERROR_MESSAGE,
                        JOptionPane.ERROR_MESSAGE);
            }
        } else {
            //we are in editiong mode    
            BSMSAppointment appointment = new BSMSAppointment();
            appointment.setAid(this.bundle.getAppointmentID());
            appointment.setResource((BSMSResource) employeeCombo.getSelectedItem());
            appointment.setClient((BSMSClient) clientCombo.getSelectedItem());
            appointment.setStatus(this.bundle.getStatus());

            Calendar calendar = dateChooser.getCalendar();
            int hour = 0;
            if (amRadioButton.isSelected()) {
                hour = (int) timeHoursSpinner.getValue();
            } else if (pmRadioButton.isSelected()) {
                hour = (int) timeHoursSpinner.getValue() + 12;
            }
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, (int) timeMinutesSpinner.getValue());

            Date currentDate = new Date(calendar.getTimeInMillis());

            calendar.add(Calendar.HOUR, (int) durationHoursSpinner.getValue());
            calendar.add(Calendar.MINUTE, (int) durationMinutesSpinner.getValue());
            Date dateAfterDuration = new Date(calendar.getTimeInMillis());

            appointment.setDateTime(new LocalDateTime(currentDate.getTime()));
            appointment.setDuration(new Duration(currentDate.getTime(), dateAfterDuration.getTime()));

            List<BSMSService> services = new ArrayList<>();
            double amount = 0;
            for (int i = 0; i < servicesList.getSelectedValuesList().size(); i++) {
                services.add((BSMSService) servicesList.getSelectedValuesList().get(i));
                amount += ((BSMSService) servicesList.getSelectedValuesList().get(i)).getPrice();
            }
            appointment.setServices(services);
            appointment.setAmount(amount);

            startingHoursCalendar = Calendar.getInstance();
            startingHoursCalendar.setTime(settings.getStartTime());

            endingHoursCalendar = Calendar.getInstance();
            endingHoursCalendar.setTime(settings.getEndTime());

            int settingsStartHours = startingHoursCalendar.get(Calendar.HOUR_OF_DAY);
            int settingsEndHours = endingHoursCalendar.get(Calendar.HOUR_OF_DAY);

            int appTime = (int) timeHoursSpinner.getValue();
            if (pmRadioButton.isSelected()) {
                if (appTime != 12) {
                    appTime += 12;
                }
            }

            if (!(appTime < settingsStartHours)
                    && !(appTime >= settingsEndHours)) {
                parentFrame.deleteAppointment(appointment);
                BSMSAppointmentTools.updateAppointment(appointment);
                if ((currentDate.getYear() + 1900) == mainViewCalendar.get(Calendar.YEAR)
                        && currentDate.getDate() == mainViewCalendar.get(Calendar.DAY_OF_MONTH)
                        && currentDate.getMonth() == mainViewCalendar.get(Calendar.MONTH)) {
                    parentFrame.addNewAppointment(appointment);
                }
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(this,
                        APPOINTMENT_TIME_NOT_WITHIN_WORKING_HOURS,
                        ERROR_MESSAGE,
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_createButtonActionPerformed

    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed
        reset();
    }//GEN-LAST:event_resetButtonActionPerformed

    private void servicesListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_servicesListMouseClicked
        int hoursAmount = 0;
        int minutesAmount = 0;
        int hoursResult = 0;
        int minutesResult = 0;

        List<BSMSService> selectedValuesList = servicesList.getSelectedValuesList();
        for (int i = 0; i < selectedValuesList.size(); i++) {
            BSMSService currentService = selectedValuesList.get(i);
            hoursAmount += currentService.getHoursItTakes();
            minutesAmount += currentService.getMinutesItTakes();
        }
        hoursResult = hoursAmount;
        if (minutesAmount >= 60) {
            hoursResult += minutesAmount / 60;
            minutesResult = minutesAmount % 60;
        } else {
            minutesResult = minutesAmount;
        }
        durationHoursSpinner.setValue(hoursResult);
        durationMinutesSpinner.setValue(minutesResult);
    }//GEN-LAST:event_servicesListMouseClicked

    private void reset() {
        resourcesModel.setSelectedItem(resourcesModel.getElementAt(0));
        clientsModel.setSelectedItem(clientsModel.getElementAt(0));
        servicesList.setSelectedIndex(0);
        dateChooser.setDate(null);

        startingHoursCalendar = Calendar.getInstance();
        startingHoursCalendar.setTime(settings.getStartTime());

        int hoursToSet = startingHoursCalendar.get(Calendar.HOUR_OF_DAY);
        if (hoursToSet == 0) {
            timeHoursSpinner.setValue((int) (hoursToSet + 12));
            amRadioButton.setSelected(true);
            pmRadioButton.setSelected(false);
        } else if (hoursToSet > 12) {
            timeHoursSpinner.setValue((int) (hoursToSet - 12));
            amRadioButton.setSelected(false);
            pmRadioButton.setSelected(true);
        } else if (hoursToSet < 12) {
            timeHoursSpinner.setValue((hoursToSet));
            amRadioButton.setSelected(true);
            pmRadioButton.setSelected(false);
        } else if (hoursToSet == 12) {
            timeHoursSpinner.setValue((int) (hoursToSet));
            amRadioButton.setSelected(false);
            pmRadioButton.setSelected(true);
        }
        timeMinutesSpinner.setValue((int) startingHoursCalendar.get(Calendar.MINUTE));

        durationHoursSpinner.setValue((int) 1);
        durationMinutesSpinner.setValue((double) 0);

        createButton.setText("create");

        this.editingMode = false;
        this.bundle = null;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton amRadioButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox clientCombo;
    private javax.swing.JButton createButton;
    private com.toedter.calendar.JDateChooser dateChooser;
    private javax.swing.JSpinner durationHoursSpinner;
    private javax.swing.JSpinner durationMinutesSpinner;
    private javax.swing.JComboBox employeeCombo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton pmRadioButton;
    private javax.swing.JButton resetButton;
    private javax.swing.JList servicesList;
    private javax.swing.JSpinner timeHoursSpinner;
    private javax.swing.JSpinner timeMinutesSpinner;
    // End of variables declaration//GEN-END:variables
}
