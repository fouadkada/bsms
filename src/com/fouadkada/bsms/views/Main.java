/*
 * To change this template, choose BSMSResourcesTools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.views;

import com.fouadkada.bsms.BSMSComponetsFactory.BSMSComponentsFactory;
import com.fouadkada.bsms.BSMSCustomComponents.BSMSAppointmentComponent;
import com.fouadkada.bsms.BSMSCustomComponents.BSMSDeferedPaymentDialog;
import com.fouadkada.bsms.BSMSCustomComponents.BSMSDiscountDialog;
import com.fouadkada.bsms.Listeners.BSMSAppointmentListener;
import com.fouadkada.bsms.Listeners.BSMSSettingsChangedListener;
import com.fouadkada.bsms.controllers.BSMSAppointmentTools;
import com.fouadkada.bsms.controllers.BSMSSettingsTools;
import com.fouadkada.bsms.controllers.DataBundle;
import com.fouadkada.bsms.models.BSMSAccounting;
import com.fouadkada.bsms.models.BSMSAppointment;
import com.fouadkada.bsms.models.BSMSResource;
import com.fouadkada.bsms.models.BSMSScheduleModel;
import com.fouadkada.bsms.models.BSMSSettings;
import com.fouadkada.bsms.views.clients_management.ClientsManagement;
import com.fouadkada.bsms.views.inventory_management.InventoryManagement;
import com.fouadkada.bsms.views.reporting.Reporting;
import com.fouadkada.bsms.views.resource_management.ResourceManagement;
import com.fouadkada.bsms.views.services_management.ServicesManagement;
import com.fouadkada.bsms.views.settings_management.SettingsManagement;
import com.thirdnf.resourceScheduler.Appointment;
import com.thirdnf.resourceScheduler.Resource;
import com.thirdnf.resourceScheduler.ScheduleListener;
import com.thirdnf.resourceScheduler.Scheduler;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;

/**
 *
 * @author fouad
 */
public class Main extends javax.swing.JFrame implements BSMSSettingsChangedListener {

    private Scheduler scheduler = null;
    private BSMSScheduleModel scheduleModel;
    private BSMSSettings settings;
    private ArrayList<BSMSAppointment> appointments = null;
    private DefaultListModel<BSMSAppointment> infoListModel = new DefaultListModel<>();
    private final String CONFIRM_TITLE = "Confirm";
    private final String CONFIRM_DELETE_APPOINTMENT = "Are you sure you want to delete this appointment?";
    private final String CONFIRM_PAY_CREDIT = "Are you sure you want to put this payment on credit?";
    private final String CONFIRM_PAY_PROCEED = "Are you sure you want to proceed with payment?";

    /**
     * Creates new form Main
     */
    public Main() {
        Calendar c = Calendar.getInstance();
        appointments = BSMSAppointmentTools.getAppointments(c);
        settings = BSMSSettingsTools.getSettings();
        setupScheduleComponent();
        initComponents();
        dateChooser.setCalendar(c);
        dateChooser.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (!evt.getOldValue().equals(evt.getNewValue())) {
                    deleteAppointments();
                    appointments = BSMSAppointmentTools.getAppointments(dateChooser.getCalendar());
                    for (int i = 0; i < appointments.size(); i++) {
                        scheduleModel.addAppointment(appointments.get(i));
                    }
                    //empty the info list after date change
                    infoListModel.clear();
                }
            }
        });
        schedulePanel.setLayout(new BorderLayout());
        schedulePanel.add(scheduler, BorderLayout.CENTER);
    }

    private void setupScheduleComponent() {
        scheduler = new Scheduler();
        scheduler.addScheduleListener(new ScheduleListener() {
            @Override
            public void actionPerformed(Resource resource, DateTime time) {
                //do nothing for now
            }
        });
        BSMSComponentsFactory bcf = new BSMSComponentsFactory();
        bcf.setAppointmentListener(new BSMSAppointmentListener() {
            @Override
            public void handleClick(Appointment appointment,
                    final BSMSAppointmentComponent appointmentComponent,
                    MouseEvent event) {
                Main.this.resetPaymentTools();
                final BSMSAppointment selectedAppointment = (BSMSAppointment) appointment;
                if (event.getButton() == MouseEvent.BUTTON1) {
                    infoListModel.addElement(selectedAppointment);
                    if (!selectedAppointment.getStatus().equalsIgnoreCase("paid")
                            && !selectedAppointment.getStatus().equalsIgnoreCase("on credit")) {                        
                        payNowButton.setEnabled(true);
                        payLaterButton.setEnabled(true);
                    }
                    resetButton.setEnabled(true);
                } else if (event.getButton() == MouseEvent.BUTTON3) {
                    JMenuItem editAppointment = new JMenuItem("Edit");
                    editAppointment.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            SwingUtilities.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    DataBundle bundle = new DataBundle();

                                    bundle.setAppointmentID(selectedAppointment.getAid());
                                    bundle.setSelectedResource((BSMSResource) selectedAppointment.getResource());
                                    bundle.setSelectedClient(selectedAppointment.getClient());
                                    bundle.setSelectedServices(selectedAppointment.getServices());
                                    bundle.setSelectedDate(selectedAppointment.getDateTime());
                                    if (selectedAppointment.getDateTime().getHourOfDay() > 12) {
                                        bundle.setSelectedHour(selectedAppointment.getDateTime().getHourOfDay() - 12);
                                        bundle.setAmSelected(false);
                                    } else {
                                        bundle.setSelectedHour(selectedAppointment.getDateTime().getHourOfDay());
                                        bundle.setAmSelected(true);
                                    }
                                    bundle.setSelectedMinute(selectedAppointment.getDateTime().getMinuteOfHour());
                                    bundle.setDurationHours((int) selectedAppointment.getDuration().getStandardHours());
                                    bundle.setDurationMinutes((int) selectedAppointment.getDuration().getStandardMinutes() % 60);
                                    bundle.setStatus(selectedAppointment.getStatus());

                                    NewAppointmentDialog nad = new NewAppointmentDialog(true,
                                            bundle,
                                            Main.this.settings,
                                            dateChooser.getCalendar());
                                    nad.setParentFrame(Main.this);
                                    nad.setVisible(true);
                                }
                            });
                        }
                    });

                    JMenuItem deleteAppointment = new JMenuItem("Delete");
                    deleteAppointment.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            int choice = JOptionPane.showConfirmDialog(appointmentComponent,
                                    CONFIRM_DELETE_APPOINTMENT,
                                    CONFIRM_TITLE,
                                    JOptionPane.YES_NO_OPTION,
                                    JOptionPane.INFORMATION_MESSAGE);
                            if (choice == JOptionPane.YES_OPTION) {
                                BSMSAppointmentTools.deleteAppointment(selectedAppointment);
                                deleteAppointment(selectedAppointment);
                                //clear the deleted appointment
                                infoListModel.clear();
                            }
                        }
                    });

                    popupMenu.removeAll();
                    popupMenu.add(editAppointment);
                    popupMenu.add(deleteAppointment);
                    popupMenu.show(appointmentComponent, event.getX(), event.getY());
                }
            }
        });
        scheduler.setComponentFactory(bcf);
        scheduleModel = new BSMSScheduleModel(this.appointments);
        scheduleModel.setSettings(settings);
        scheduler.setModel(scheduleModel);
        scheduler.showDate(new LocalDate());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popupMenu = new javax.swing.JPopupMenu();
        jToolBar1 = new javax.swing.JToolBar();
        resourceManagmentButton = new javax.swing.JButton();
        clientsManagementButton = new javax.swing.JButton();
        servicesManagementButton = new javax.swing.JButton();
        inventoryButton = new javax.swing.JButton();
        reportsButton = new javax.swing.JButton();
        settingsButton = new javax.swing.JButton();
        toolsPanel = new javax.swing.JPanel();
        newAppointmentButton = new javax.swing.JButton();
        dateChooser = new com.toedter.calendar.JDateChooser();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        infoList = new javax.swing.JList();
        payLaterButton = new javax.swing.JButton();
        payNowButton = new javax.swing.JButton();
        resetButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        schedulePanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("BSMS");

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        resourceManagmentButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/pictures/people.png"))); // NOI18N
        resourceManagmentButton.setText("Employees Management");
        resourceManagmentButton.setFocusable(false);
        resourceManagmentButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        resourceManagmentButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        resourceManagmentButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resourceManagmentButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(resourceManagmentButton);

        clientsManagementButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/pictures/clients.png"))); // NOI18N
        clientsManagementButton.setText("Clients Management");
        clientsManagementButton.setFocusable(false);
        clientsManagementButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        clientsManagementButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        clientsManagementButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clientsManagementButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(clientsManagementButton);

        servicesManagementButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/pictures/services.png"))); // NOI18N
        servicesManagementButton.setText("Services Management");
        servicesManagementButton.setFocusable(false);
        servicesManagementButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        servicesManagementButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        servicesManagementButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                servicesManagementButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(servicesManagementButton);

        inventoryButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/pictures/inventory.png"))); // NOI18N
        inventoryButton.setText("Inventory Management");
        inventoryButton.setFocusable(false);
        inventoryButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        inventoryButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        inventoryButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inventoryButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(inventoryButton);

        reportsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/pictures/reports.png"))); // NOI18N
        reportsButton.setText("Reports");
        reportsButton.setFocusable(false);
        reportsButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        reportsButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        reportsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportsButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(reportsButton);

        settingsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/pictures/settings.png"))); // NOI18N
        settingsButton.setText("Settings");
        settingsButton.setFocusable(false);
        settingsButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        settingsButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        settingsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                settingsButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(settingsButton);

        newAppointmentButton.setText("New Appointment");
        newAppointmentButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newAppointmentButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout toolsPanelLayout = new javax.swing.GroupLayout(toolsPanel);
        toolsPanel.setLayout(toolsPanelLayout);
        toolsPanelLayout.setHorizontalGroup(
            toolsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(toolsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(newAppointmentButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        toolsPanelLayout.setVerticalGroup(
            toolsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(toolsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(toolsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(dateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(newAppointmentButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jSplitPane1.setDividerLocation(200);

        infoList.setModel(infoListModel);
        infoList.setCellRenderer(new InfoListCellRenderer());
        jScrollPane2.setViewportView(infoList);

        payLaterButton.setText("Pay Later");
        payLaterButton.setEnabled(false);
        payLaterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                payLaterButtonActionPerformed(evt);
            }
        });

        payNowButton.setText("Pay Now");
        payNowButton.setEnabled(false);
        payNowButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                payNowButtonActionPerformed(evt);
            }
        });

        resetButton.setText("Reset");
        resetButton.setEnabled(false);
        resetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
            .addComponent(payNowButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(payLaterButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
            .addComponent(resetButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 522, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(payNowButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(payLaterButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(resetButton))
        );

        jSplitPane1.setLeftComponent(jPanel2);

        javax.swing.GroupLayout schedulePanelLayout = new javax.swing.GroupLayout(schedulePanel);
        schedulePanel.setLayout(schedulePanelLayout);
        schedulePanelLayout.setHorizontalGroup(
            schedulePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 613, Short.MAX_VALUE)
        );
        schedulePanelLayout.setVerticalGroup(
            schedulePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 607, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(schedulePanel);

        jSplitPane1.setRightComponent(jScrollPane1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 821, Short.MAX_VALUE)
            .addComponent(toolsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(toolsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSplitPane1))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void resourceManagmentButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resourceManagmentButtonActionPerformed
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ResourceManagement rm = new ResourceManagement(Main.this);
                rm.setVisible(true);
            }
        });
    }//GEN-LAST:event_resourceManagmentButtonActionPerformed

    private void clientsManagementButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clientsManagementButtonActionPerformed
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ClientsManagement cm = new ClientsManagement();
                cm.setVisible(true);
            }
        });
    }//GEN-LAST:event_clientsManagementButtonActionPerformed

    private void servicesManagementButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_servicesManagementButtonActionPerformed
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ServicesManagement sm = new ServicesManagement();
                sm.setVisible(true);
            }
        });
    }//GEN-LAST:event_servicesManagementButtonActionPerformed

    private void newAppointmentButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newAppointmentButtonActionPerformed
        NewAppointmentDialog nad = new NewAppointmentDialog(
                false,
                null,
                this.settings,
                dateChooser.getCalendar());
        nad.setParentFrame(Main.this);
        nad.setVisible(true);
    }//GEN-LAST:event_newAppointmentButtonActionPerformed

    private void settingsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_settingsButtonActionPerformed
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                SettingsManagement sm = new SettingsManagement(Main.this, settings);
                sm.addListener(Main.this);
                sm.setVisible(true);
            }
        });
    }//GEN-LAST:event_settingsButtonActionPerformed

    private void payNowButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_payNowButtonActionPerformed
        int choice = JOptionPane.showConfirmDialog(this,
                CONFIRM_PAY_PROCEED,
                CONFIRM_TITLE,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.INFORMATION_MESSAGE);
        if (choice == JOptionPane.YES_OPTION) {
            BSMSAppointment selectedAppointment = infoListModel.getElementAt(0);
            selectedAppointment.setAccounting(new BSMSAccounting());
            BSMSDiscountDialog dd = new BSMSDiscountDialog(this, selectedAppointment.getAmount());
            int discountAmout = dd.getDiscountAmount();
            if (discountAmout != -1) {
                selectedAppointment.setStatus("paid");
                selectedAppointment.getAccounting().setDiscount(discountAmout);
                selectedAppointment.getAccounting().setAmountPaid(dd.getFinalAmount());
                selectedAppointment.getAccounting().setAmountToPayAfterDiscount(dd.getFinalAmount());
                BSMSAppointmentTools.payAppointment(selectedAppointment);
                Main.this.resetPaymentTools();
            }
        }
    }//GEN-LAST:event_payNowButtonActionPerformed

    private void payLaterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_payLaterButtonActionPerformed
        int choice = JOptionPane.showConfirmDialog(this,
                CONFIRM_PAY_CREDIT,
                CONFIRM_TITLE,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.INFORMATION_MESSAGE);
        if (choice == JOptionPane.YES_OPTION) {
            BSMSAppointment selectedAppointment = infoListModel.getElementAt(0);
            selectedAppointment.setAccounting(new BSMSAccounting());
            BSMSDiscountDialog dd = new BSMSDiscountDialog(this, selectedAppointment.getAmount());
            int discountAmout = dd.getDiscountAmount();
            if (discountAmout != -1) {
                selectedAppointment.getAccounting().setAmountToPayAfterDiscount(dd.getFinalAmount());
                BSMSDeferedPaymentDialog dpd = new BSMSDeferedPaymentDialog(this, dd.getFinalAmount());
                if (dpd.getAmountPaid() != -1) {
                    if (dpd.getAmountPaid() == dd.getFinalAmount()) {
                        selectedAppointment.setStatus("paid");
                    } else {
                        selectedAppointment.setStatus("on credit");
                    }
                    selectedAppointment.getAccounting().setAmountPaid(dpd.getAmountPaid());
                    BSMSAppointmentTools.payAppointment(selectedAppointment);
                    Main.this.resetPaymentTools();
                }
            }
        }
    }//GEN-LAST:event_payLaterButtonActionPerformed

    private void resetPaymentTools() {
        infoListModel.removeAllElements();
        payNowButton.setEnabled(false);
        payLaterButton.setEnabled(false);
        resetButton.setEnabled(false);
    }

    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed
        resetPaymentTools();
    }//GEN-LAST:event_resetButtonActionPerformed

    private void reportsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportsButtonActionPerformed
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Reporting reporting = new Reporting();
                reporting.setVisible(true);
            }
        });
    }//GEN-LAST:event_reportsButtonActionPerformed

    private void inventoryButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inventoryButtonActionPerformed
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                InventoryManagement inventory = new InventoryManagement();
                inventory.setVisible(true);
            }
        });
    }//GEN-LAST:event_inventoryButtonActionPerformed

    protected void addNewAppointment(BSMSAppointment newAppointment) {
        scheduleModel.addAppointment(newAppointment);
        appointments.add(newAppointment);
    }

    protected void deleteAppointment(BSMSAppointment appointment) {
        scheduleModel.deleteAppointment(appointment);
        appointments.remove(appointment);
    }

    private void deleteAppointments() {
        scheduleModel.deleteAppointments();
        int numOfAppointment = appointments.size();
        for (int i = numOfAppointment - 1; i >= 0; i--) {
            appointments.remove(i);
        }
    }

    protected void updateAppointment(BSMSAppointment appointment) {
        scheduleModel.deleteAppointment(appointment);
        appointments.remove(appointment);
        scheduleModel.addAppointment(appointment);
        appointments.add(appointment);
    }

    public void addNewResource(BSMSResource newResource) {
        scheduleModel.addNewResource(newResource);
    }

    public void deleteResource(BSMSResource resource) {
        scheduleModel.deleteResource(resource);
    }

    public void updateResource(BSMSResource resource) {
        scheduleModel.updateResource(resource);
    }

    @Override
    public void startDateChanged(Date newDate) {
        settings.setStartTime(newDate);
        scheduleModel.startTimeChanged(settings.getStartTime(), settings.getEndTime());
    }

    @Override
    public void endDateChanged(Date newDate) {
        settings.setEndTime(newDate);
        scheduleModel.endTimeChanged(settings.getStartTime(), settings.getEndTime());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                        if ("Nimbus".equals(info.getName())) {
                            UIManager.setLookAndFeel(info.getClassName());
                            break;
                        }
                    }
                    UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
                new Main().setVisible(true);
            }
        });
    }

    class InfoListCellRenderer implements ListCellRenderer<BSMSAppointment> {

        @Override
        public Component getListCellRendererComponent(JList<? extends BSMSAppointment> list, BSMSAppointment value, int index, boolean isSelected, boolean cellHasFocus) {
            JPanel p = new JPanel();
            p.setBackground(Color.WHITE);
            p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

            JTextArea text = new JTextArea();
            text.setText(value.toString());

            p.add(text);
            return p;
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton clientsManagementButton;
    private com.toedter.calendar.JDateChooser dateChooser;
    private javax.swing.JList infoList;
    private javax.swing.JButton inventoryButton;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JButton newAppointmentButton;
    private javax.swing.JButton payLaterButton;
    private javax.swing.JButton payNowButton;
    private javax.swing.JPopupMenu popupMenu;
    private javax.swing.JButton reportsButton;
    private javax.swing.JButton resetButton;
    private javax.swing.JButton resourceManagmentButton;
    private javax.swing.JPanel schedulePanel;
    private javax.swing.JButton servicesManagementButton;
    private javax.swing.JButton settingsButton;
    private javax.swing.JPanel toolsPanel;
    // End of variables declaration//GEN-END:variables
}
