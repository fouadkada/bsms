/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.BSMSCustomComponents;

import com.fouadkada.bsms.models.BSMSResource;
import com.thirdnf.resourceScheduler.components.BasicResourceComponent;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author fouad
 */
public class BSMSResourceComponent extends BasicResourceComponent {

    private BSMSResource resource;

    public BSMSResourceComponent(BSMSResource resource) {
        super(resource);
        this.resource = resource;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
    }
}
