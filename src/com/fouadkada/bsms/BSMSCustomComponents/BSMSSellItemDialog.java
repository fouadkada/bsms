/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.BSMSCustomComponents;

import com.fouadkada.bsms.models.BSMSSellItemDataBundle;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 *
 * @author fouad
 */
public class BSMSSellItemDialog extends JDialog {

    private BSMSSellItemDataBundle dataBundle = null;
    private BSMSSellItemDataBundle selectedItem = null;

    public BSMSSellItemDialog(JFrame parent, BSMSSellItemDataBundle selectedItem) {
        super(parent, true);
        this.selectedItem = selectedItem;
        initComponents();
        setLocationRelativeTo(parent);
        pack();
        setResizable(false);
        setVisible(true);
    }

    private void initComponents() {
        this.setLayout(new BorderLayout());
        this.add(new BSMSSellItemDialogPanel(BSMSSellItemDialog.this, selectedItem));
    }

    protected void disposeDialog() {
        setVisible(false);
    }

    protected void setDataBundle(BSMSSellItemDataBundle dataBundle) {
        this.dataBundle = dataBundle;
    }

    public BSMSSellItemDataBundle getDataBundle() {
        return dataBundle;
    }
}
