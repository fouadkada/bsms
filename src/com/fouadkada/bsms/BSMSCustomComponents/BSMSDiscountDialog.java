/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.BSMSCustomComponents;

import com.fouadkada.bsms.views.Main;
import java.awt.BorderLayout;
import javax.swing.JDialog;

/**
 *
 * @author fouad
 */
public class BSMSDiscountDialog extends JDialog {

    private int discountAmount;
    private double appointmentAmount, finalAmount;

    public BSMSDiscountDialog(Main parent, double appointmentAmount) {
        super(parent, true);
        this.appointmentAmount = appointmentAmount;
        initComponents();
        pack();
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setResizable(false);
        setVisible(true);
    }

    private void initComponents() {
        this.setLayout(new BorderLayout());
        this.add(new BSMSDiscountDialogPanel(BSMSDiscountDialog.this, appointmentAmount), BorderLayout.CENTER);
    }

    protected void disposeDialog() {
        this.setVisible(false);
    }

    /**
     * @return the discountAmount
     */
    public int getDiscountAmount() {
        return discountAmount;
    }

    /**
     * @param discountAmount the discountAmount to set
     */
    public void setDiscountAmount(int discountAmount) {
        this.discountAmount = discountAmount;
    }

    /**
     * @return the finalAmount
     */
    public double getFinalAmount() {
        return finalAmount;
    }

    /**
     * @param finalAmount the finalAmount to set
     */
    public void setFinalAmount(double finalAmount) {
        this.finalAmount = finalAmount;
    }
}
