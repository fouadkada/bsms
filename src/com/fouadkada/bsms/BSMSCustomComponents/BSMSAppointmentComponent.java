/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.BSMSCustomComponents;

import com.fouadkada.bsms.Listeners.BSMSAppointmentListener;
import com.fouadkada.bsms.models.BSMSAppointment;
import com.fouadkada.bsms.models.BSMSResource;
import com.thirdnf.resourceScheduler.Appointment;
import com.thirdnf.resourceScheduler.components.BasicAppointmentComponent;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fouad
 */
public class BSMSAppointmentComponent extends BasicAppointmentComponent implements MouseListener {

    private BSMSAppointment appointment;
    private BSMSAppointmentListener appointmentListener;

    public BSMSAppointmentComponent(BSMSAppointment appointment) {
        super(appointment);
        this.appointment = appointment;
        addMouseListener(BSMSAppointmentComponent.this);
    }

    @Override
    protected void paintComponent(Graphics g) {
        try {
            Field f = Class.forName("java.awt.Color").getField(((BSMSResource) (appointment.getResource())).getColor());
            setBackground((Color) f.get(null));
            super.paintComponent(g);
        } catch (ClassNotFoundException | NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(BSMSAppointmentComponent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointmentListener(BSMSAppointmentListener appointmentListener) {
        this.appointmentListener = appointmentListener;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (appointmentListener != null) {
            appointmentListener.handleClick(appointment, BSMSAppointmentComponent.this,  e);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
