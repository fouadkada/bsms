/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.BSMSCustomComponents;

import com.fouadkada.bsms.views.Main;
import com.fouadkada.bsms.views.reporting.AccountingReportPanel;
import java.awt.BorderLayout;
import java.awt.Dialog;
import javax.swing.JDialog;

/**
 *
 * @author fouad
 */
public class BSMSDeferedPaymentDialog extends JDialog {

    private double initialAmount;
    private double amountPaid;

    public BSMSDeferedPaymentDialog(Main parent, double initialAmount) {
        super(parent, true);
        this.initialAmount = initialAmount;
        initComponents();
        setLocationRelativeTo(parent);
        pack();
        setResizable(false);
        setVisible(true);
    }

    private void initComponents() {
        this.setLayout(new BorderLayout());
        this.add(new BSMSDeferedPaymentPanel(this, initialAmount), BorderLayout.CENTER);
    }

    protected void disposeDialog() {
        setVisible(false);
    }

    /**
     * @return the amountPaid
     */
    public double getAmountPaid() {
        return amountPaid;
    }

    /**
     * @param amountPaid the amountPaid to set
     */
    public void setAmountPaid(double amountPaid) {
        this.amountPaid = amountPaid;
    }
}
