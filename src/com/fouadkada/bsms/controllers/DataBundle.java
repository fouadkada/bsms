/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.controllers;

import com.fouadkada.bsms.models.BSMSClient;
import com.fouadkada.bsms.models.BSMSResource;
import com.fouadkada.bsms.models.BSMSService;
import java.util.List;
import org.joda.time.LocalDateTime;

/**
 *
 * @author fouad
 */
public class DataBundle {

    private long appointmentID;
    private BSMSResource selectedResource;
    private BSMSClient selectedClient;
    private List<BSMSService> selectedServices;
    private LocalDateTime selectedDate;
    private int selectedHour, selectedMinute;
    private boolean amSelected;
    private int durationHours, durationMinutes;    
    private String status;

    /**
     * @return the selectedResource
     */
    public BSMSResource getSelectedResource() {
        return selectedResource;
    }

    /**
     * @param selectedResource the selectedResource to set
     */
    public void setSelectedResource(BSMSResource selectedResource) {
        this.selectedResource = selectedResource;
    }

    /**
     * @return the selectedClient
     */
    public BSMSClient getSelectedClient() {
        return selectedClient;
    }

    /**
     * @param selectedClient the selectedClient to set
     */
    public void setSelectedClient(BSMSClient selectedClient) {
        this.selectedClient = selectedClient;
    }

    public List<BSMSService> getSelectedServices() {
        return selectedServices;
    }

    public void setSelectedServices(List<BSMSService> selectedServices) {
        this.selectedServices = selectedServices;
    }

    /**
     * @return the selectedDate
     */
    public LocalDateTime getSelectedDate() {
        return selectedDate;
    }

    /**
     * @param selectedDate the selectedDate to set
     */
    public void setSelectedDate(LocalDateTime selectedDate) {
        this.selectedDate = selectedDate;
    }

    /**
     * @return the selectedHour
     */
    public int getSelectedHour() {
        return selectedHour;
    }

    /**
     * @param selectedHour the selectedHour to set
     */
    public void setSelectedHour(int selectedHour) {
        this.selectedHour = selectedHour;
    }

    /**
     * @return the selectedMinute
     */
    public int getSelectedMinute() {
        return selectedMinute;
    }

    /**
     * @param selectedMinute the selectedMinute to set
     */
    public void setSelectedMinute(int selectedMinute) {
        this.selectedMinute = selectedMinute;
    }

    /**
     * @return the amSelected
     */
    public boolean isAmSelected() {
        return amSelected;
    }

    /**
     * @param amSelected the amSelected to set
     */
    public void setAmSelected(boolean amSelected) {
        this.amSelected = amSelected;
    }

    /**
     * @return the durationHours
     */
    public int getDurationHours() {
        return durationHours;
    }

    /**
     * @param durationHours the durationHours to set
     */
    public void setDurationHours(int durationHours) {
        this.durationHours = durationHours;
    }

    /**
     * @return the durationMinutes
     */
    public int getDurationMinutes() {
        return durationMinutes;
    }

    /**
     * @param durationMinutes the durationMinutes to set
     */
    public void setDurationMinutes(int durationMinutes) {
        this.durationMinutes = durationMinutes;
    }

    /**
     * @return the appointmentID
     */
    public long getAppointmentID() {
        return appointmentID;
    }

    /**
     * @param appointmentID the appointmentID to set
     */
    public void setAppointmentID(long appointmentID) {
        this.appointmentID = appointmentID;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
}
