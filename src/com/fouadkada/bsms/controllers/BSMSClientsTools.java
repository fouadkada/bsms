/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.controllers;

import com.fouadkada.bsms.models.BSMSClient;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.h2.jdbcx.JdbcConnectionPool;

/**
 *
 * @author fouad
 */
public class BSMSClientsTools {

    private static String dbHost = Config.dbHost;
    private static String dbUsername = Config.dbUsername;
    private static String dbPassword = Config.dbPassword;

    public static ArrayList<BSMSClient> getClients() {
        try {
            ArrayList<BSMSClient> clients = new ArrayList<>();

            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "SELECT cid, LOWER(fname) AS fname, LOWER(lname) AS lname, phone1, phone2 FROM CLIENTS ORDER BY fname";
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                BSMSClient client = new BSMSClient(rs.getString("fname"),
                        rs.getString("lname"),
                        rs.getString("phone1"),
                        rs.getString("phone2"));
                client.setCid(rs.getInt("cid"));
                clients.add(client);
            }
            rs.close();
            stmt.close();
            con.close();

            return clients;
        } catch (SQLException ex) {
            Logger.getLogger(BSMSClientsTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    //add client

    public static void addClient(BSMSClient newClient) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "INSERT INTO Clients(FNAME, LNAME, PHONE1, PHONE2) VALUES(?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, newClient.getFname());
            stmt.setString(2, newClient.getLname());
            stmt.setString(3, newClient.getPhone1());
            stmt.setString(4, newClient.getPhone2());
            stmt.executeUpdate();

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSClientsTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * checks if the client we are trying to add already exists, if the client
     * doesn't exist then we can add otherwise the client cannot be added.
     *
     * @param newClient the client to add
     */
    public static boolean checkIfClientExistAndAdd(BSMSClient newClient) {
        boolean result = false;
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            int count = 0;
            String query = "SELECT COUNT(1) as COUNT "
                    + "FROM CLIENTS "
                    + "WHERE ((phone1=? OR phone2=?) AND phone1 != '') "
                    + "OR ((phone2=? OR phone1=?) AND phone2!= '')";                                    
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, newClient.getPhone1());
            stmt.setString(2, newClient.getPhone1());
            stmt.setString(3, newClient.getPhone2());
            stmt.setString(4, newClient.getPhone2());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                count = rs.getInt("COUNT");
                if (count > 0) {
                    return true;
                }
            }

            query = "INSERT INTO Clients(FNAME, LNAME, PHONE1, PHONE2) VALUES(?, ?, ?, ?)";
            stmt = con.prepareStatement(query);
            stmt.setString(1, newClient.getFname());
            stmt.setString(2, newClient.getLname());
            stmt.setString(3, newClient.getPhone1());
            stmt.setString(4, newClient.getPhone2());
            stmt.executeUpdate();

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSClientsTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    //delete client, im not sure about the code, in the try there is execute update is it right?
    // after writing it, do we need to delete client?? when will he use this??
    public static void deleteClient(BSMSClient client) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "Delete FROM Clients WHERE CID=?";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, client.getCid());

            stmt.executeUpdate();

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSClientsTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void updateClient(BSMSClient updatedClient) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "UPDATE CLIENTS SET FNAME=?, LNAME=?, PHONE1=?, PHONE2=? WHERE CID=?";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, updatedClient.getFname());
            stmt.setString(2, updatedClient.getLname());
            stmt.setString(3, updatedClient.getPhone1());
            stmt.setString(4, updatedClient.getPhone2());
            stmt.setInt(5, updatedClient.getCid());

            stmt.executeUpdate();

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSClientsTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
