/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.controllers;

import com.fouadkada.bsms.views.Main;
import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author fouad
 */
public class Tools {

    public static double convertToDecimal(int hours, double minutes) {
        return (double) (hours + (minutes / 60));
    }

    public static double[] breakToHoursAndMinutes(double amount) {
        double[] result = new double[2];
        if (amount < 1) {
            result[0] = 0;
            result[1] = amount * 100;
        } else {
            System.out.println("sdfdf" + (int) amount);
            result[0] = Math.floor(amount / 60);
            result[1] = (amount % 60);
        }
        return result;
    }

    public static int convertTo24Hours(int hours, boolean pm) {
        if (pm) {
            return 12 + hours;
        }
        return hours;
    }

    public static void restartApplication() throws IOException, InterruptedException {
        StringBuilder cmd = new StringBuilder();
        cmd.append(System.getProperty("java.home")).append(File.separator).append("bin").append(File.separator).append("java ");
        for (String jvmArg : ManagementFactory.getRuntimeMXBean().getInputArguments()) {
            cmd.append(jvmArg).append(" ");
        }
        cmd.append("-cp ").append(ManagementFactory.getRuntimeMXBean().getClassPath()).append(" ");
        cmd.append(Main.class.getName()).append(" ");

        Thread.sleep(1000); // 1 second delay before restart
        Runtime.getRuntime().exec(cmd.toString());
        System.exit(0);
    }

    public static String formatDate(String pattern, Date dateToFormat) {
        SimpleDateFormat sdf;
        if (pattern == null) {
            pattern = "d-MM-YYYY h:mm a";
            sdf = new SimpleDateFormat(pattern);
            return sdf.format(dateToFormat);
        } else {
            sdf = new SimpleDateFormat(pattern);
            return sdf.format(dateToFormat);
        }
    }
    
    
}
