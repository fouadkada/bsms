/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.controllers;

import java.util.Date;

/**
 *
 * @author fouad
 */
public class BSMSAccountingPaymentCombination {

    private double amountPaid;
    private Date datePaid;
    private int aid;

    public BSMSAccountingPaymentCombination() {
        amountPaid = -1;
        datePaid = null;
        aid = -1;
    }

    public BSMSAccountingPaymentCombination(double amountPaid, Date datePaid) {
        this.amountPaid = amountPaid;
        this.datePaid = datePaid;
        aid = -1;
    }

    /**
     * @return the amountPaid
     */
    public double getAmountPaid() {
        return amountPaid;
    }

    /**
     * @param amountPaid the amountPaid to set
     */
    public void setAmountPaid(double amountPaid) {
        this.amountPaid = amountPaid;
    }

    /**
     * @return the datePaid
     */
    public Date getDatePaid() {
        return datePaid;
    }

    /**
     * @param datePaid the datePaid to set
     */
    public void setDatePaid(Date datePaid) {
        this.datePaid = datePaid;
    }

    /**
     * @return the aid
     */
    public int getAid() {
        return aid;
    }

    /**
     * @param aid the aid to set
     */
    public void setAid(int aid) {
        this.aid = aid;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BSMSAccountingPaymentCombination) {
            BSMSAccountingPaymentCombination newC = (BSMSAccountingPaymentCombination) obj;
            if (this.aid == newC.getAid()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + this.aid;
        return hash;
    }
}
