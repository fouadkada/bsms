/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.controllers;

import com.fouadkada.bsms.models.BSMSItem;
import com.fouadkada.bsms.models.BSMSSellItemDataBundle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.h2.jdbcx.JdbcConnectionPool;

/**
 *
 * @author fouad
 */
public class BSMSInventoryTools {

    private static String dbHost = Config.dbHost;
    private static String dbUsername = Config.dbUsername;
    private static String dbPassword = Config.dbPassword;

    public static int addItem(BSMSItem newItem) {
        int newItemID = 0;
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "INSERT INTO ITEMS(DESCRIPTION, PRICE, QUANTITY) VALUES(?,?,?)";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, newItem.getDescription());
            stmt.setDouble(2, newItem.getPrice());
            stmt.setInt(3, newItem.getQuantity());

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                newItemID = rs.getInt(1);
            }

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSInventoryTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return newItemID;
    }

    public static ArrayList<BSMSItem> getAllItems() {
        ArrayList<BSMSItem> items = new ArrayList<>();
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "SELECT * FROM ITEMS";
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                BSMSItem item = new BSMSItem(rs.getString("description"), rs.getDouble("price"), rs.getInt("quantity"));
                item.setIid(rs.getInt("iid"));
                items.add(item);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BSMSInventoryTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return items;
    }

    public static void updateItem(BSMSItem updatedItem) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "UPDATE ITEMS SET DESCRIPTION=?, PRICE=?, QUANTITY=? WHERE IID=?";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, updatedItem.getDescription());
            stmt.setDouble(2, updatedItem.getPrice());
            stmt.setInt(3, updatedItem.getQuantity());
            stmt.setInt(4, updatedItem.getIid());

            stmt.executeUpdate();

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSInventoryTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void deleteItem(BSMSItem item) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "DELETE FROM ITEMS WHERE IID=?";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, item.getIid());

            stmt.executeUpdate();

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSInventoryTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void sellItem(BSMSSellItemDataBundle dataBundle) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "INSERT INTO SOLD_ITEMS(DESCRIPTION, PRICE, QUANTITY, SELL_DATE) VALUES(?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, dataBundle.getDescription());
            stmt.setDouble(2, dataBundle.getAmount());
            stmt.setInt(3, dataBundle.getQuantity());
            stmt.setTimestamp(4, new Timestamp(Calendar.getInstance().getTimeInMillis()));

            stmt.executeUpdate();

            query = "UPDATE ITEMS SET QUANTITY = QUANTITY - " + dataBundle.getQuantity() + " WHERE IID='" + dataBundle.getIid() + "\'";
            stmt = con.prepareStatement(query);

            stmt.executeUpdate();

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSInventoryTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static ArrayList<BSMSSellItemDataBundle> runInventoryReportingQuery(String query) {
        ArrayList<BSMSSellItemDataBundle> entries = new ArrayList<>();
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            BSMSSellItemDataBundle entry;
            while (rs.next()) {
                entry = new BSMSSellItemDataBundle();
                entry.setDescription(rs.getString("DESCRIPTION"));
                entry.setAmount(rs.getDouble("PRICE"));
                entry.setQuantity(rs.getInt("QUANTITY"));
                entry.setIid(rs.getInt("SIID"));
                entry.setTimeStamp(rs.getTimestamp("SELL_DATE"));
                entries.add(entry);
            }

            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSInventoryTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return entries;
    }
}
