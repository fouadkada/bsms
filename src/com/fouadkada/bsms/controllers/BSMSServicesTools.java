/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.controllers;

import com.fouadkada.bsms.models.BSMSService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.h2.jdbcx.JdbcConnectionPool;

/**
 *
 * @author Sako
 */
public class BSMSServicesTools {

    private static String dbHost = Config.dbHost;
    private static String dbUsername = Config.dbUsername;
    private static String dbPassword = Config.dbPassword;

    //getservices
    public static ArrayList<BSMSService> getServices() {
        try {
            ArrayList<BSMSService> services = new ArrayList<>();

            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "SELECT sid, name, price, hoursittakes, minutesittakes FROM SERVICES ORDER BY name";
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                BSMSService service = new BSMSService(rs.getString("name"),
                        rs.getDouble("price"),
                        rs.getInt("hoursittakes"),
                        rs.getInt("minutesittakes"));
                service.setSid(rs.getInt("sid"));
                services.add(service);
            }
            rs.close();
            stmt.close();
            con.close();

            return services;
        } catch (SQLException ex) {
            Logger.getLogger(BSMSServicesTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    //addservice
    public static void addService(BSMSService newService) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "INSERT INTO Services(NAME, PRICE, HOURSITTAKES, MINUTESITTAKES) VALUES(?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, newService.getName());
            stmt.setDouble(2, newService.getPrice());
            stmt.setDouble(3, newService.getHoursItTakes());
            stmt.setDouble(4, newService.getMinutesItTakes());
            stmt.executeUpdate();

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSServicesTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //deleteservice
    public static void deleteService(BSMSService service) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "Delete FROM Services WHERE SID=?";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, service.getSid());

            stmt.executeUpdate();

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSServicesTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //updateservice
    public static void updateService(BSMSService updatedService) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "UPDATE SERVICES SET NAME=?, PRICE=?, HOURSITTAKES=?, MINUTESITTAKES=? WHERE SID=?";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, updatedService.getName());
            stmt.setDouble(2, updatedService.getPrice());
            stmt.setDouble(3, updatedService.getHoursItTakes());
            stmt.setDouble(4, updatedService.getMinutesItTakes());
            stmt.setInt(5, updatedService.getSid());

            stmt.executeUpdate();

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSResourcesTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
