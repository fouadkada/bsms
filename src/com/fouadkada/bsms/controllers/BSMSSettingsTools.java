/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.controllers;

import com.fouadkada.bsms.models.BSMSSettings;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.h2.jdbcx.JdbcConnectionPool;

/**
 *
 * @author fouad
 */
public class BSMSSettingsTools {

    private static String dbHost = Config.dbHost;
    private static String dbUsername = Config.dbUsername;
    private static String dbPassword = Config.dbPassword;

    public static BSMSSettings getSettings() {
        try {
            BSMSSettings settings = new BSMSSettings();
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "SELECT * FROM SETTINGS";
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                settings.setSid(rs.getInt("sid"));
                settings.setStartTime(rs.getTimestamp("STARTTIME"));
                settings.setEndTime(rs.getTimestamp("ENDTIME"));
            }

            rs.close();
            stmt.close();
            con.close();

            return settings;
        } catch (SQLException ex) {
            Logger.getLogger(Tools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void setSettings(BSMSSettings settings) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "INSERT INTO SETTINGS(STARTTIME, ENDTIME) VALUES(?, ?)";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setTimestamp(1, new Timestamp(settings.getStartTime().getTime()));
            stmt.setTimestamp(2, new Timestamp(settings.getEndTime().getTime()));
            stmt.executeUpdate();

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Tools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
