/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.controllers;

import com.fouadkada.bsms.models.BSMSAccounting;
import com.fouadkada.bsms.models.BSMSAppointment;
import com.fouadkada.bsms.models.BSMSClient;
import com.fouadkada.bsms.models.BSMSResource;
import com.fouadkada.bsms.models.BSMSService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.h2.jdbcx.JdbcConnectionPool;
import org.joda.time.Duration;
import org.joda.time.LocalDateTime;

/**
 *
 * @author fouad
 */
public class BSMSAppointmentTools {

    private static String dbHost = Config.dbHost;
    private static String dbUsername = Config.dbUsername;
    private static String dbPassword = Config.dbPassword;

    public static long addAppointment(BSMSAppointment newAppointment) {
        long result = -1;
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "INSERT INTO APPOINTMENTS(RID, CID, APPOINTMEMT_DATE, DURATION_IN_MINUTES, AMOUNT, STATUS) "
                    + "VALUES(?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            stmt.setInt(1, ((BSMSResource) (newAppointment.getResource())).getRid());
            stmt.setInt(2, ((BSMSClient) (newAppointment.getClient())).getCid());

            LocalDateTime dateTime = newAppointment.getDateTime();
            stmt.setTimestamp(3, new Timestamp(new java.util.Date(dateTime.getYear() - 1900,
                    dateTime.getMonthOfYear() - 1,
                    dateTime.getDayOfMonth(),
                    dateTime.getHourOfDay(),
                    dateTime.getMinuteOfHour()).getTime()));
            stmt.setLong(4, newAppointment.getDuration().getStandardMinutes());
            stmt.setDouble(5, newAppointment.getAmount());
            stmt.setString(6, newAppointment.getStatus());

            stmt.executeUpdate();

            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                BSMSService service = null;
                for (int i = 0; i < newAppointment.getServices().size(); i++) {
                    service = newAppointment.getServices().get(i);
                    query = "INSERT INTO APPOINTMENT_SERVICES(AID, SID) "
                            + "VALUES(?, ?)";
                    stmt = con.prepareStatement(query);

                    stmt.setLong(1, generatedKeys.getLong(1));
                    stmt.setInt(2, service.getSid());

                    stmt.executeUpdate();
                }
            }

            result = generatedKeys.getLong(1);

            generatedKeys.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSClientsTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public static void updateAppointment(BSMSAppointment appointment) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            //update existing appointment.
            String query = "UPDATE APPOINTMENTS SET RID=?, "
                    + "CID=?, "
                    + "APPOINTMEMT_DATE=?, "
                    + "DURATION_IN_MINUTES=?, "
                    + "AMOUNT=? "
                    + "WHERE AID=?";
            PreparedStatement stmt = con.prepareStatement(query);

            stmt.setInt(1, ((BSMSResource) (appointment.getResource())).getRid());
            stmt.setInt(2, ((BSMSClient) (appointment.getClient())).getCid());

            LocalDateTime dateTime = appointment.getDateTime();
            stmt.setTimestamp(3, new Timestamp(new java.util.Date(dateTime.getYear() - 1900,
                    dateTime.getMonthOfYear() - 1,
                    dateTime.getDayOfMonth(),
                    dateTime.getHourOfDay(),
                    dateTime.getMinuteOfHour()).getTime()));
            stmt.setLong(4, appointment.getDuration().getStandardMinutes());
            stmt.setDouble(5, appointment.getAmount());
            stmt.setLong(6, appointment.getAid());

            stmt.executeUpdate();

            //delete existing services as these might have changed.
            query = "DELETE FROM APPOINTMENT_SERVICES WHERE AID=?";
            stmt = con.prepareStatement(query);
            stmt.setLong(1, appointment.getAid());
            stmt.executeUpdate();

            //re-insert services as new services might be different than old ones.
            BSMSService service;
            for (int i = 0; i < appointment.getServices().size(); i++) {
                service = appointment.getServices().get(i);
                query = "INSERT INTO APPOINTMENT_SERVICES(AID, SID) "
                        + "VALUES(?, ?)";
                stmt = con.prepareStatement(query);

                stmt.setLong(1, appointment.getAid());
                stmt.setInt(2, service.getSid());

                stmt.executeUpdate();
            }

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSClientsTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void deleteAppointment(BSMSAppointment appointment) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "DELETE FROM APPOINTMENTS WHERE AID=?";
            PreparedStatement stmt = con.prepareStatement(query);

            stmt.setLong(1, appointment.getAid());

            stmt.executeUpdate();

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSClientsTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static ArrayList<BSMSAppointment> getAppointments(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        long range1 = calendar.getTimeInMillis();

        calendar.add(Calendar.HOUR_OF_DAY, 23);
        calendar.add(Calendar.MINUTE, 59);
        calendar.add(Calendar.SECOND, 59);

        long range2 = calendar.getTimeInMillis();

        ArrayList<BSMSAppointment> appointments = new ArrayList<>();
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "SELECT APPOINTMENTS.AID, "
                    + "RESOURCES.RID, "
                    + "RESOURCES.FNAME, "
                    + "RESOURCES.LNAME,"
                    + "RESOURCES.COLOR, "
                    + "CLIENTS.CID, "
                    + "CLIENTS.FNAME, "
                    + "CLIENTS.LNAME, "
                    + "CLIENTS.PHONE1, "
                    + "CLIENTS.PHONE2, "
                    + "APPOINTMENTS.APPOINTMEMT_DATE, "
                    + "APPOINTMENTS.DURATION_IN_MINUTES, "
                    + "APPOINTMENTS.AMOUNT, "
                    + "APPOINTMENTS.STATUS,"
                    + "SERVICES.SID,"
                    + "SERVICES.NAME,"
                    + "SERVICES.PRICE "
                    + "FROM APPOINTMENTS "
                    + "INNER JOIN RESOURCES "
                    + "ON APPOINTMENTS.RID = RESOURCES.RID "
                    + "INNER JOIN CLIENTS "
                    + "ON APPOINTMENTS.CID = CLIENTS.CID "
                    + "INNER JOIN APPOINTMENT_SERVICES "
                    + "ON APPOINTMENTS.AID = APPOINTMENT_SERVICES.AID "
                    + "INNER JOIN SERVICES "
                    + "ON SERVICES.SID = APPOINTMENT_SERVICES.SID "
                    + "WHERE APPOINTMENTS.APPOINTMEMT_DATE BETWEEN ? AND ?";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setTimestamp(1, new Timestamp(range1));
            stmt.setTimestamp(2, new Timestamp(range2));
            ResultSet rs = stmt.executeQuery();

            long lastAID = -1;
            BSMSAppointment appointment = new BSMSAppointment();
            List<BSMSService> services = new ArrayList<>();

            while (rs.next()) {
                int currentAID = rs.getInt("APPOINTMENTS.AID");
                if (currentAID != lastAID) {
                    //reset the appointment and services as this is a new appointment.
                    services = new ArrayList<>();
                    appointment = new BSMSAppointment();

                    appointment.setAid(currentAID);
                    appointment.setAmount(rs.getDouble("APPOINTMENTS.AMOUNT"));
                    appointment.setDateTime(new LocalDateTime(rs.getTimestamp("APPOINTMENTS.APPOINTMEMT_DATE").getTime()));
                    appointment.setDuration(Duration.standardMinutes(rs.getInt("APPOINTMENTS.DURATION_IN_MINUTES")));
                    appointment.setStatus(rs.getString("STATUS"));

                    BSMSClient client = new BSMSClient();
                    client.setCid(rs.getInt("CLIENTS.CID"));
                    client.setFname(rs.getString("CLIENTS.FNAME"));
                    client.setLname(rs.getString("CLIENTS.LNAME"));
                    client.setPhone1(rs.getString("CLIENTS.PHONE1"));
                    client.setPhone2(rs.getString("CLIENTS.PHONE2"));
                    appointment.setClient(client);

                    BSMSResource resource = new BSMSResource();
                    resource.setRid(rs.getInt("RESOURCES.RID"));
                    resource.setFname(rs.getString("RESOURCES.FNAME"));
                    resource.setLname(rs.getString("RESOURCES.LNAME"));
                    resource.setColor(rs.getString("RESOURCES.COLOR"));
                    appointment.setResource(resource);

                    BSMSService service = new BSMSService();
                    service.setSid(rs.getInt("SERVICES.SID"));
                    service.setAid(currentAID);
                    service.setName(rs.getString("SERVICES.NAME"));
                    service.setPrice(rs.getDouble("SERVICES.PRICE"));
                    services.add(service);
                    appointment.setServices(services);

                    appointments.add(appointment);

                    lastAID = currentAID;
                } else {
                    BSMSService service = new BSMSService();
                    service.setSid(rs.getInt("SERVICES.SID"));
                    service.setAid(currentAID);
                    service.setName(rs.getString("SERVICES.NAME"));
                    service.setPrice(rs.getDouble("SERVICES.PRICE"));
                    services.add(service);
                    appointment.setServices(services);
                    lastAID = rs.getLong("APPOINTMENTS.AID");
                }
            }

            for (int i = 0; i < appointments.size(); i++) {
                BSMSAppointment currentAppointment = appointments.get(i);
                BSMSAccounting accounting = new BSMSAccounting();

                query = "SELECT AMOUNTPAID, AMOUNTAFTERDISCOUNT FROM ACCOUNTING WHERE APPID = ?";
                stmt = con.prepareStatement(query);
                stmt.setLong(1, currentAppointment.getAid());

                rs = stmt.executeQuery();
                while (rs.next()) {
                    accounting.setAppointmentID(currentAppointment.getAid());
                    accounting.setAmountPaid(rs.getDouble("AMOUNTPAID"));
                    accounting.setAmountToPayAfterDiscount(rs.getDouble("AMOUNTAFTERDISCOUNT"));
                }
                currentAppointment.setAccounting(accounting);
            }

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSClientsTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return appointments;
    }

    public static void payAppointment(BSMSAppointment updatedAppointment) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "UPDATE APPOINTMENTS SET STATUS=? WHERE AID=?";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, updatedAppointment.getStatus());
            stmt.setLong(2, updatedAppointment.getAid());
            stmt.executeUpdate();

            query = "INSERT INTO ACCOUNTING(APPID, AMOUNTPAID, AMOUNTAFTERDISCOUNT ,DATEPAID) "
                    + "VALUES(?, ?, ?, ?)";
            stmt = con.prepareStatement(query);
            stmt.setLong(1, updatedAppointment.getAid());
            stmt.setDouble(2, updatedAppointment.getAccounting().getAmountPaid());
            stmt.setDouble(3, updatedAppointment.getAccounting().getAmountToPayAfterDiscount());
            stmt.setTimestamp(4, new Timestamp(new Date().getTime()));
            stmt.executeUpdate();

            stmt.close();
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(BSMSAppointmentTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
