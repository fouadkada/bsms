/*
 * To change this template, choose BSMSResourcesTools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.controllers;

import com.fouadkada.bsms.models.BSMSResource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.h2.jdbcx.JdbcConnectionPool;

/**
 *
 * @author fouad kada
 */
public class BSMSResourcesTools {

    private static String dbHost = Config.dbHost;
    private static String dbUsername = Config.dbUsername;
    private static String dbPassword = Config.dbPassword;

    public static ArrayList<BSMSResource> getActiveResources() {
        try {
            ArrayList<BSMSResource> resources = new ArrayList<BSMSResource>();

            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "SELECT * FROM RESOURCES WHERE STATUS != FALSE";
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                BSMSResource resource = new BSMSResource(rs.getString("fname"), rs.getString("lname"));
                resource.setRid(rs.getInt("rid"));
                resource.setColor(rs.getString("color"));
                resource.setStatus(rs.getBoolean("status"));
                resources.add(resource);
            }

            rs.close();
            stmt.close();
            con.close();

            return resources;
        } catch (SQLException ex) {
            Logger.getLogger(BSMSResourcesTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void addResource(BSMSResource newResource) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "INSERT INTO RESOURCES(FNAME, LNAME, COLOR, STATUS) VALUES(?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, newResource.getFname());
            stmt.setString(2, newResource.getLname());
            stmt.setString(3, newResource.getColor());
            stmt.setBoolean(4, newResource.getStatus());
            stmt.executeUpdate();

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSResourcesTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void deleteResource(BSMSResource resource) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "UPDATE RESOURCES SET STATUS=FALSE WHERE RID=?";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, resource.getRid());

            stmt.executeUpdate();

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSResourcesTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void updateResource(BSMSResource updatedResource) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();
            String query = "UPDATE RESOURCES SET FNAME=?, LNAME=?, COLOR=? WHERE RID=?";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, updatedResource.getFname());
            stmt.setString(2, updatedResource.getLname());
            stmt.setString(3, updatedResource.getColor());
            stmt.setInt(4, updatedResource.getRid());

            stmt.executeUpdate();

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSResourcesTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
