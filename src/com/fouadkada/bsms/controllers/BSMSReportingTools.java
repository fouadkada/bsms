/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.controllers;

import com.fouadkada.bsms.models.BSMSAccountingReport;
import com.fouadkada.bsms.models.BSMSClient;
import com.fouadkada.bsms.models.BSMSReport;
import com.fouadkada.bsms.models.BSMSResource;
import com.fouadkada.bsms.models.BSMSService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.h2.jdbcx.JdbcConnectionPool;

/**
 *
 * @author fouad
 */
public class BSMSReportingTools {

    private static String dbHost = Config.dbHost;
    private static String dbUsername = Config.dbUsername;
    private static String dbPassword = Config.dbPassword;

    public static ArrayList<BSMSReport> runReportingQuery(String query, String servicesConditions) {
        ArrayList<BSMSReport> reports = new ArrayList<>();
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();

            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();

            BSMSReport report;
            ArrayList<BSMSService> services = null;
            int aid = 0;
            while (rs.next()) {
                if (rs.getInt("AID") != aid) {
                    aid = rs.getInt("AID");

                    report = new BSMSReport();

                    report.setAppointmentID(aid);
                    report.setClient(new BSMSClient(rs.getString("CFNAME"), rs.getString("CLNAME"), rs.getString("PHONE1"), rs.getString("PHONE2")));
                    report.setAppointmentDate(new Date(rs.getTimestamp("APPOINTMENTS.APPOINTMEMT_DATE").getTime()));
                    report.setResource(new BSMSResource(rs.getString("RFNAME"), rs.getString("RLNAME")));
                    report.setStatus(rs.getString("APPOINTMENTS.STATUS"));
                    services = getServicesByAID(aid, servicesConditions);
                    if (!services.isEmpty()) {
                        report.setServices(services);
                        reports.add(report);
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(BSMSReportingTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return reports;
    }

    private static ArrayList<BSMSService> getServicesByAID(int aid, String servicesConditions) {
        ArrayList<BSMSService> services = new ArrayList<>();
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();

            String query = "SELECT NAME "
                    + "FROM SERVICES "
                    + "INNER JOIN APPOINTMENT_SERVICES "
                    + "ON APPOINTMENT_SERVICES.SID = SERVICES.SID "
                    + "INNER JOIN APPOINTMENTS "
                    + "ON APPOINTMENTS.AID = APPOINTMENT_SERVICES.AID "
                    + "AND APPOINTMENT_SERVICES.AID = " + aid + " " + servicesConditions;

            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();

            BSMSService service;
            while (rs.next()) {
                service = new BSMSService();
                service.setName(rs.getString("name"));
                services.add(service);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BSMSReportingTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return services;
    }

    public static ArrayList<BSMSAccountingReport> runAccountingReportingQuery(String query, String servicesConditions) {
        ArrayList<BSMSAccountingReport> reports = new ArrayList<>();
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();

            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();

            BSMSAccountingReport report;
            ArrayList<BSMSService> services = null;
            int aid = 0;
            while (rs.next()) {
                if (rs.getInt("APPAID") != aid) {
                    aid = rs.getInt("APPAID");

                    report = new BSMSAccountingReport();

                    report.setAppointmentID(aid);
                    report.setClient(new BSMSClient(rs.getString("CFNAME"), rs.getString("CLNAME"), rs.getString("PHONE1"), rs.getString("PHONE2")));
                    report.setAppointmentDate(new Date(rs.getTimestamp("APPOINTMENTS.APPOINTMEMT_DATE").getTime()));
                    report.setResource(new BSMSResource(rs.getString("RFNAME"), rs.getString("RLNAME")));
                    report.setStatus(rs.getString("APPOINTMENTS.STATUS"));
                    report.setAmountBeforeDiscount(rs.getDouble("APPOINTMENTS.AMOUNT"));
                    services = getServicesByAID(aid, servicesConditions);
                    report.setServices(services);
                    if (!services.isEmpty()) {
                        reports.add(report);
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(BSMSReportingTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return reports;
    }

    public static void runExtendedAccountingQuery(BSMSAccountingReport report, String query) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();

            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            ArrayList<BSMSAccountingPaymentCombination> combinations = new ArrayList<>();
            while (rs.next()) {
                report.setAmountAfterDiscount(rs.getDouble("AMOUNTAFTERDISCOUNT"));
                BSMSAccountingPaymentCombination combination = new BSMSAccountingPaymentCombination(rs.getDouble("AMOUNTPAID"), new Date(rs.getTimestamp("DATEPAID").getTime()));
                combination.setAid(rs.getInt("AID"));
                combinations.add(combination);
            }
            report.setCombinations(combinations);

            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSReportingTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void deletePayment(BSMSAccountingPaymentCombination accId) {
        try {
            JdbcConnectionPool cp = JdbcConnectionPool.create(dbHost, dbUsername, dbPassword);
            Connection con = cp.getConnection();

            String query = "DELETE FROM ACCOUNTING WHERE AID=?";

            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, accId.getAid());
            stmt.executeUpdate();

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BSMSReportingTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
