/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models;

import com.thirdnf.resourceScheduler.Appointment;
import com.thirdnf.resourceScheduler.Resource;
import java.util.List;
import org.joda.time.Duration;
import org.joda.time.LocalDateTime;

/**
 *
 * @author fouad
 */
public class BSMSAppointment implements Appointment {

    private long aid;
    private BSMSResource resource;
    private BSMSClient client;
    private List<BSMSService> services;
    private Duration duration;
    private LocalDateTime dateTime;
    private double amount;
    private String status;
    private BSMSAccounting accounting;

    @Override
    public String getTitle() {
        return getClient().getFname() + " " + getClient().getLname();
    }

    @Override
    public LocalDateTime getDateTime() {
        return dateTime;
    }

    @Override
    public Resource getResource() {
        return resource;
    }

    @Override
    public Duration getDuration() {
        return duration;
    }

    /**
     * @return the aid
     */
    public long getAid() {
        return this.aid;
    }

    /**
     * @param aid the aid to set
     */
    public void setAid(long aid) {
        this.aid = aid;
    }

    /**
     * @param resource the resource to set
     */
    public void setResource(BSMSResource resource) {
        this.resource = resource;
    }

    /**
     * @return the client
     */
    public BSMSClient getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(BSMSClient client) {
        this.client = client;
    }

    /**
     * @return the service
     */
    public List<BSMSService> getServices() {
        return services;
    }

    /**
     * @param service the service to set
     */
    public void setServices(List<BSMSService> services) {
        this.services = services;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    /**
     * @param dateTime the dateTime to set
     */
    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return this.amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        List<BSMSService> servicesList = getServices();
        String servicesRepresentation = "";
        double amountToDisplay = 0;
        for (int i = 0; i < servicesList.size(); i++) {
            servicesRepresentation += servicesList.get(i).toStringRepresentation() + "\n";
            amountToDisplay += servicesList.get(i).getPrice();
        }        

        double hoursToShow = getDuration().getStandardHours();
        double minutesToShow = (getDuration().getStandardMinutes()) - (hoursToShow * 60);       

        return "Employee: " + ((BSMSResource) getResource()).toStringRepresentation() + "\n"
                + "Client: " + getClient().toStringRepresentation() + "\n"
                + "Services:\n" + servicesRepresentation
                + "Original Amount: " + amountToDisplay + "\n"
                + "Duration: " + (int) hoursToShow + "h " + (int) minutesToShow + "m" + "\n"
                + "Status: " + getStatus();
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BSMSAppointment) {
            BSMSAppointment appointment = (BSMSAppointment) obj;
            if (this.aid == appointment.getAid()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + (int) (this.aid ^ (this.aid >>> 32));
        return hash;
    }

    /**
     * @return the accounting
     */
    public BSMSAccounting getAccounting() {
        return accounting;
    }

    /**
     * @param accounting the accounting to set
     */
    public void setAccounting(BSMSAccounting accounting) {
        this.accounting = accounting;
    }
}
