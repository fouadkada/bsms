/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models;

import com.fouadkada.bsms.controllers.Tools;

/**
 *
 * @author Sako
 */
public class BSMSService {

    private int sid, hoursItTakes, minutesItTakes;
    private long aid;
    private String name;
    private double price;

    public BSMSService() {
        this.sid = -1;
        this.aid = -1;
        this.name = "";
        this.price = -1;
        this.hoursItTakes = 0;
        this.minutesItTakes = 0;
    }

    public BSMSService(String name, double price, int hoursItTakes, int minutesItTakes) {
        this.name = name;
        this.price = price;
        this.hoursItTakes = hoursItTakes;
        this.minutesItTakes = minutesItTakes;
    }

    /**
     * @return the sid
     */
    public int getSid() {
        return sid;
    }

    /**
     * @param sid the sid to set
     */
    public void setSid(int sid) {
        this.sid = sid;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return getName();
    }

    public String toStringRepresentation() {
        return "Service: " + getName() + "\n"
                + "Price: " + getPrice() + "\n";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BSMSService other = (BSMSService) obj;
        if (this.sid != other.sid) {
            return false;
        }
        return true;
    }
    // the hashcode 

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.sid;
        return hash;
    }

    /**
     * @return the aid
     */
    public long getAid() {
        return this.aid;
    }

    /**
     * @param aid the aid to set
     */
    public void setAid(long aid) {
        this.aid = aid;
    }

    /**
     * @return the hoursItTakes
     */
    public int getHoursItTakes() {
        return hoursItTakes;
    }

    /**
     * @param hoursItTakes the hoursItTakes to set
     */
    public void setHoursItTakes(int hoursItTakes) {
        this.hoursItTakes = hoursItTakes;
    }

    /**
     * @return the minutesItTakes
     */
    public int getMinutesItTakes() {
        return minutesItTakes;
    }

    /**
     * @param minutesItTakes the minutesItTakes to set
     */
    public void setMinutesItTakes(int minutesItTakes) {
        this.minutesItTakes = minutesItTakes;
    }
}
