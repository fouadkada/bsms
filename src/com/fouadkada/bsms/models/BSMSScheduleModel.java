/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models;

import com.fouadkada.bsms.controllers.BSMSResourcesTools;
import com.thirdnf.resourceScheduler.AbstractScheduleModel;
import com.thirdnf.resourceScheduler.AppointmentVisitor;
import com.thirdnf.resourceScheduler.Availability;
import com.thirdnf.resourceScheduler.ResourceVisitor;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.Duration;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

/**
 *
 * @author fouad
 */
public class BSMSScheduleModel extends AbstractScheduleModel {

    private List<BSMSResource> resources = null;
    private List<BSMSAppointment> appointments = null;
    private BSMSSettings settings;

    public BSMSScheduleModel(ArrayList<BSMSAppointment> appointments) {
        resources = BSMSResourcesTools.getActiveResources();
        this.appointments = new ArrayList<>(appointments);
    }

    public void addAppointment(BSMSAppointment newAppointment) {
        appointments.add(newAppointment);
        fireAppointmentAdded(newAppointment);
    }

    public void deleteAppointment(BSMSAppointment appointment) {
        appointments.remove(appointment);
        fireAppointmentRemoved(appointment);
    }

    public void deleteAppointments() {
        int numOfAppointments = appointments.size();
        for (int i = numOfAppointments - 1; i >= 0; i--) {
            BSMSAppointment appointmentToRemove = appointments.get(i);
            appointments.remove(i);
            fireAppointmentRemoved(appointmentToRemove);
        }
    }

    public void updateAppointment(BSMSAppointment appointment) {
        appointments.remove(appointment);
        appointments.add(appointment);
        fireAppointmentUpdated(appointment);
    }

    public void addNewResource(BSMSResource newResource) {
        resources.add(newResource);
        fireResourceAdded(newResource, new LocalDate(Calendar.getInstance().getTime()), -1);
    }

    public void deleteResource(BSMSResource resource) {
        resources.remove(resource);
        fireResourceRemoved(resource, new LocalDate(Calendar.getInstance().getTime()));
    }

    public void updateResource(BSMSResource resource) {
        int index = resources.indexOf(resource);
        if (index != -1) {
            BSMSResource oldValue = resources.get(index);
            oldValue.setFname(resource.getFname());
            oldValue.setLname(resource.getLname());
            oldValue.setColor(resource.getColor());
            oldValue.setAvailability(resource.getAvailability());
            oldValue.setStatus(resource.getStatus());
            fireResourceUpdated(oldValue);
        }
    }

    public void startTimeChanged(Date startDate, Date endDate) {
        handleScheduleChanges(startDate, endDate);
    }

    public void endTimeChanged(Date startDate, Date endDate) {
        handleScheduleChanges(startDate, endDate);
    }

    private void handleScheduleChanges(Date startDate, Date endDate) {
        List<Availability> availabilities = new ArrayList<>();
        availabilities.add(new Availability(new LocalTime(startDate.getTime()),
                new Duration(startDate.getTime(), endDate.getTime())));
        for (int i = 0; i < resources.size(); i++) {
            resources.get(i).setAvailability(availabilities);
            fireResourceUpdated(resources.get(i));
        }
    }

    @Override
    public void visitAppointments(AppointmentVisitor visitor, LocalDate dateTime) {
        if (appointments != null) {
            for (int i = 0; i < appointments.size(); i++) {
                visitor.visitAppointment(appointments.get(i));
            }
        }
    }

    @Override
    public void visitResources(ResourceVisitor visitor, LocalDate dateTime) {
        if (resources != null) {
            for (int i = 0; i < resources.size(); i++) {
                visitor.visitResource(resources.get(i));
            }
        }
    }

    @Override
    public LocalTime getStartTime(LocalDate dateTime) {
        return new LocalTime(settings.getStartTime().getTime());
    }

    @Override
    public LocalTime getEndTime(LocalDate dateTime) {
        return new LocalTime(settings.getEndTime().getTime());
    }

    /**
     * @param settings the settings to set
     */
    public void setSettings(BSMSSettings settings) {
        this.settings = settings;
    }
}