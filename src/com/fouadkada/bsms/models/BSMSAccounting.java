/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models;

import org.joda.time.LocalDateTime;

/**
 *
 * @author fouad
 */
public class BSMSAccounting {

    private int AID;
    private long appID;
    private double amountPaid, amountToPayAfterDiscount;
    private int discount;
    private LocalDateTime datePaid;

    public BSMSAccounting() {
        this.AID = 0;
        this.appID = 0;
        this.amountPaid = 0;
        this.amountToPayAfterDiscount = 0;
        this.datePaid = null;
    }

    public BSMSAccounting(int AID, long appID, double amountPaid, double amountToPayAfterDiscount, LocalDateTime datePaid) {
        this.AID = AID;
        this.appID = appID;
        this.amountPaid = amountPaid;
        this.amountToPayAfterDiscount = amountToPayAfterDiscount;
        this.datePaid = datePaid;
    }

    /**
     * @return the Accounting ID
     */
    public int getAID() {
        return AID;
    }

    /**
     * @param AID the Accounting ID to set
     */
    public void setAID(int AID) {
        this.AID = AID;
    }

    /**
     * @return the appointment ID
     */
    public long getAppointmentID() {
        return appID;
    }

    /**
     * @param appID the appointment ID to set
     */
    public void setAppointmentID(long appID) {
        this.appID = appID;
    }

    /**
     * @return the amountPaid
     */
    public double getAmountPaid() {
        return amountPaid;
    }

    /**
     * @param amountPaid the amountPaid to set
     */
    public void setAmountPaid(double amountPaid) {
        this.amountPaid = amountPaid;
    }

    /**
     * @return the amountToPay
     */
    public double getAmountToPayAfterDiscount() {
        return amountToPayAfterDiscount;
    }

    /**
     * @param amountToPayAfterDiscount the amountToPay to set
     */
    public void setAmountToPayAfterDiscount(double amountToPayAfterDiscount) {
        this.amountToPayAfterDiscount = amountToPayAfterDiscount;
    }

    /**
     * @return the datePaid
     */
    public LocalDateTime getDatePaid() {
        return datePaid;
    }

    /**
     * @param datePaid the datePaid to set
     */
    public void setDatePaid(LocalDateTime datePaid) {
        this.datePaid = datePaid;
    }

    /**
     * @return the discount
     */
    public int getDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(int discount) {
        this.discount = discount;
    }
}
