/*
 * To change this template, choose BSMSResourcesTools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models.SwingModels.ListModels;

import com.fouadkada.bsms.controllers.BSMSResourcesTools;
import com.fouadkada.bsms.models.BSMSResource;
import java.util.ArrayList;
import javax.swing.DefaultListModel;

/**
 *
 * @author fouad
 */
public class BSMSResourcesManagementListModel extends DefaultListModel<BSMSResource> {

    private ArrayList<BSMSResource> resources = null;

    public BSMSResourcesManagementListModel() {
        resources = BSMSResourcesTools.getActiveResources();
    }

    @Override
    public int getSize() {
        return resources.size();
    }

    @Override
    public BSMSResource getElementAt(int index) {
        return resources.get(index);
    }

    @Override
    public boolean isEmpty() {
        return resources.isEmpty();
    }

    @Override
    public void addElement(BSMSResource element) {
        BSMSResourcesTools.addResource(element);
        resources.add(element);
        fireIntervalAdded(this, 0, resources.size());
    }

    @Override
    public void setElementAt(BSMSResource element, int index) {
        BSMSResourcesTools.updateResource(element);
        resources.set(index, element);        
        fireContentsChanged(this, 0, resources.size());
    }

    @Override
    public boolean removeElement(Object obj) {
        if (obj instanceof BSMSResource) {
            BSMSResource resourceToDelete = (BSMSResource) obj;
            BSMSResourcesTools.deleteResource(resourceToDelete);
            resources.remove(resourceToDelete);
            fireIntervalRemoved(this, 0, resources.size());
        }
        return false;
    }
}
