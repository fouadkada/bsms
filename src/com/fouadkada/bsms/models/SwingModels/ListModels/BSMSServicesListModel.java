/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models.SwingModels.ListModels;

import com.fouadkada.bsms.controllers.BSMSServicesTools;
import com.fouadkada.bsms.models.BSMSService;
import java.util.ArrayList;
import javax.swing.DefaultListModel;

/**
 *
 * @author fouad
 */
public class BSMSServicesListModel extends DefaultListModel<BSMSService> {

    private ArrayList<BSMSService> services;

    public BSMSServicesListModel() {
        services = BSMSServicesTools.getServices();
    }

    @Override
    public int getSize() {
        return services.size();
    }

    @Override
    public int size() {
        return services.size();
    }

    @Override
    public boolean isEmpty() {
        return services.isEmpty();
    }

    @Override
    public BSMSService elementAt(int index) {
        return services.get(index);
    }

    @Override
    public BSMSService getElementAt(int index) {
        return services.get(index);
    }

    @Override
    public BSMSService firstElement() {
        return services.get(0);
    }

    @Override
    public BSMSService lastElement() {
        if (!services.isEmpty()) {
            return services.get(services.size() - 1);
        } else {
            return null;
        }
    }

    @Override
    public BSMSService get(int index) {
        return services.get(index);
    }
}
