/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models.SwingModels.ComboBoxModels;

import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author fouad
 */
public class ColoredComboBoxModel extends DefaultComboBoxModel<String> {

    private ArrayList<String> colors = new ArrayList<String>();

    public ColoredComboBoxModel() {
        colors.add("yellow");
        colors.add("blue");
        colors.add("green");
        colors.add("cyan");
        colors.add("magenta");
        colors.add("orange");
        colors.add("red");
    }

    @Override
    public int getSize() {
        return colors.size();
    }

    @Override
    public String getElementAt(int index) {
        return colors.get(index);
    }
}
