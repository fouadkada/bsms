/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models.SwingModels.ComboBoxModels;

import com.fouadkada.bsms.controllers.BSMSResourcesTools;
import com.fouadkada.bsms.models.BSMSResource;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author fouad
 */
public class BSMSResourcesComboBoxModel extends DefaultComboBoxModel<BSMSResource> {

    private ArrayList<BSMSResource> resources;
    private BSMSResource selectedResource;

    public BSMSResourcesComboBoxModel() {
        resources = BSMSResourcesTools.getActiveResources();
        selectedResource = resources.get(0);
    }

    public void addHeader() {
        BSMSResource headerResource = new BSMSResource("All", "employees");
        resources.add(0, headerResource);
        selectedResource = resources.get(0);
    }

    @Override
    public Object getSelectedItem() {
        return selectedResource;
    }

    @Override
    public void setSelectedItem(Object anObject) {
        selectedResource = (BSMSResource) anObject;
    }

    @Override
    public int getSize() {
        return resources.size();
    }

    @Override
    public BSMSResource getElementAt(int index) {
        if (resources != null) {
            return resources.get(index);
        }
        return null;
    }
}
