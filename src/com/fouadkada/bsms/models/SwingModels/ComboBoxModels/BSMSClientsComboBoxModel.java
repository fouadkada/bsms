/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models.SwingModels.ComboBoxModels;

import com.fouadkada.bsms.controllers.BSMSClientsTools;
import com.fouadkada.bsms.models.BSMSClient;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author fouad
 */
public class BSMSClientsComboBoxModel extends DefaultComboBoxModel<BSMSClient> {

    private ArrayList<BSMSClient> clients;
    private BSMSClient selectedClient;

    public BSMSClientsComboBoxModel() {
        clients = BSMSClientsTools.getClients();
        selectedClient = clients.get(0);
    }

    public void addHeader() {
        BSMSClient headerClient = new BSMSClient("All", "clients", "", "");
        clients.add(0, headerClient);
        selectedClient = clients.get(0);
    }

    @Override
    public void setSelectedItem(Object anObject) {
        selectedClient = (BSMSClient) anObject;
    }

    @Override
    public Object getSelectedItem() {
        return selectedClient;
    }

    @Override
    public int getSize() {
        return clients.size();
    }

    @Override
    public BSMSClient getElementAt(int index) {
        return clients.get(index);
    }
}
