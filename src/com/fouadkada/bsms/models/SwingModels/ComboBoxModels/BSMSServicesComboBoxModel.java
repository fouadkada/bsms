/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models.SwingModels.ComboBoxModels;

import com.fouadkada.bsms.controllers.BSMSServicesTools;
import com.fouadkada.bsms.models.BSMSService;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author fouad
 */
public class BSMSServicesComboBoxModel extends DefaultComboBoxModel<BSMSService> {

    private ArrayList<BSMSService> services;
    private BSMSService selectedService;

    public BSMSServicesComboBoxModel() {
        services = BSMSServicesTools.getServices();
        selectedService = services.get(0);
    }

    public void addHeader() {
        BSMSService headerService = new BSMSService("All services", 0, 0, 0);
        services.add(0, headerService);
        selectedService = services.get(0);
    }

    @Override
    public void setSelectedItem(Object anObject) {
        selectedService = (BSMSService) anObject;
    }

    @Override
    public Object getSelectedItem() {
        return selectedService;
    }

    @Override
    public int getSize() {
        return services.size();
    }

    @Override
    public BSMSService getElementAt(int index) {
        return services.get(index);
    }
}
