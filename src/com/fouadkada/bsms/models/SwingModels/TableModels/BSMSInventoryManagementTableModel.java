/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models.SwingModels.TableModels;

import com.fouadkada.bsms.controllers.BSMSInventoryTools;
import com.fouadkada.bsms.models.BSMSItem;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author fouad
 */
public class BSMSInventoryManagementTableModel extends DefaultTableModel {

    private ArrayList<BSMSItem> items = null;

    public BSMSInventoryManagementTableModel() {
        items = BSMSInventoryTools.getAllItems();
    }

    @Override
    public void addRow(Object[] rowData) {
        BSMSItem newItem = new BSMSItem(String.valueOf(rowData[0]), (Double) (rowData[1]), (Integer) (rowData[2]));
        newItem.setIid((Integer) (rowData[3]));
        items.add(newItem);
        fireTableRowsInserted(0, items.size());
    }

    @Override
    public void removeRow(int row) {
        items.remove(row);
        fireTableRowsDeleted(0, row);
    }

    public int getItemID(int index) {
        return items.get(index).getIid();
    }

    @Override
    public int getRowCount() {
        if (items != null) {
            return items.size();
        } else {
            return 0;
        }
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Description";
            case 1:
                return "Price";
            case 2:
                return "Quantity";
            default:
                return "";
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return true;
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return items.get(row).getDescription();
            case 1:
                return items.get(row).getPrice();
            case 2:
                return items.get(row).getQuantity();
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) throws NumberFormatException {
        switch (column) {
            case 0: {
                String newValue = (String) aValue;
                if (!newValue.equalsIgnoreCase("")) {
                    items.get(row).setDescription(newValue);
                }
                break;
            }
            case 1: {
                try {
                    double newValue = Double.valueOf(String.valueOf(aValue));
                    items.get(row).setPrice(newValue);
                    break;
                } catch (NumberFormatException ex) {
                    break;
                }
            }
            case 2: {
                try {
                    int newValue = Integer.valueOf(String.valueOf(aValue));
                    items.get(row).setQuantity(newValue);
                    break;
                } catch (NumberFormatException ex) {
                    break;
                }
            }
        }
        BSMSInventoryTools.updateItem(items.get(row));
        fireTableRowsUpdated(0, items.size());
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return items.get(0).getDescription().getClass();
            case 1:
                return Double.class;
            case 2:
                return Integer.class;
            default:
                return null;
        }
    }
}
