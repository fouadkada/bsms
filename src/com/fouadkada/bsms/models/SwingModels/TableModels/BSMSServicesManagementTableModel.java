/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models.SwingModels.TableModels;

import com.fouadkada.bsms.controllers.BSMSServicesTools;
import com.fouadkada.bsms.models.BSMSService;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sako
 */
public class BSMSServicesManagementTableModel extends DefaultTableModel {

    private ArrayList<BSMSService> services = null;

    public BSMSServicesManagementTableModel() {
        services = BSMSServicesTools.getServices();
    }

    @Override
    public int getRowCount() {
        if (services == null) {
            return 0;
        } else {
            return services.size();
        }
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Service";
            case 1:
                return "Price";
            case 2:
                return "Hours";
            case 3:
                return "Minutes";
            default:
                return "";

        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return true;
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return services.get(row).getName();
            case 1:
                return services.get(row).getPrice();
            case 2:
                return services.get(row).getHoursItTakes();
            case 3:
                return services.get(row).getMinutesItTakes();
            default:
                return "";
        }
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        switch (column) {
            case 0: {
                String newValue = (String) aValue;
                if (!newValue.equalsIgnoreCase("")) {
                    services.get(row).setName(newValue);
                }
                break;
            }
            case 1: {
                try {
                    double newValue = Double.valueOf(aValue.toString());
                    services.get(row).setPrice(newValue);
                    break;
                } catch (NumberFormatException ex) {
                    break;
                }
            }
            case 2: {
                try {
                    int newValue = Integer.valueOf(aValue.toString());
                    services.get(row).setHoursItTakes(newValue);
                    break;
                } catch (NumberFormatException ex) {
                    break;
                }
            }
            case 3: {
                try {
                    int newValue = Integer.valueOf(aValue.toString());
                    services.get(row).setMinutesItTakes(newValue);
                    break;
                } catch (NumberFormatException ex) {
                    break;
                }
            }
        }
        BSMSServicesTools.updateService(services.get(row));
        fireTableRowsUpdated(row, row);
    }

    @Override
    public void addRow(Object[] rowData) {
        services.add(new BSMSService(String.valueOf(rowData[0]),
                Double.valueOf(String.valueOf(rowData[1])),
                Integer.valueOf(String.valueOf(rowData[2])),
                Integer.valueOf(String.valueOf(rowData[3]))));
        fireTableRowsInserted(0, services.size());
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return services.get(0).getName().getClass();
            case 1:
                return Double.class;
            case 2:
                return Integer.class;
            case 3:
                return Integer.class;
            default:
                return null;
        }
    }
}
