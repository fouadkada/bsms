/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models.SwingModels.TableModels;

import com.fouadkada.bsms.controllers.BSMSAccountingPaymentCombination;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sako
 */
public class BSMSPaymentsTableModel extends DefaultTableModel {

    private ArrayList<BSMSAccountingPaymentCombination> combinations = null;

    public BSMSPaymentsTableModel(ArrayList<BSMSAccountingPaymentCombination> combinations) {
        this.combinations = combinations;
    }

    public ArrayList<BSMSAccountingPaymentCombination> getCombinations() {
        return combinations;
    }

    @Override
    public int getRowCount() {
        if (combinations == null) {
            return 0;
        } else {
            return combinations.size();
        }
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "";
            case 1:
                return "Amount";
            case 2:
                return "Date";
            default:
                return "";

        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return combinations.get(row).getAid();
            case 1:
                return combinations.get(row).getAmountPaid();
            case 2:
                return combinations.get(row).getDatePaid();
            default:
                return "";
        }
    }

    @Override
    public void addRow(Object[] rowData) {
        BSMSAccountingPaymentCombination apc = new BSMSAccountingPaymentCombination();
        apc.setAid((Integer) rowData[0]);
        apc.setAmountPaid((Double) rowData[1]);
        apc.setDatePaid((Date) rowData[2]);
        combinations.add(apc);
        fireTableRowsInserted(0, combinations.size());
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Integer.class;
            case 1:
                return Double.class;
            case 2:
                return Date.class;
            default:
                return null;
        }
    }

    @Override
    public void removeRow(int row) {
        int combinationsSize = combinations.size();
        combinations.remove(row);
        fireTableRowsDeleted(0, combinationsSize);
    }
}
