/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models.SwingModels.TableModels;

import com.fouadkada.bsms.controllers.BSMSClientsTools;
import com.fouadkada.bsms.models.BSMSClient;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sako
 */
public class BSMSClientsManagementTableModel extends DefaultTableModel {

    private ArrayList<BSMSClient> clients = null;

    public BSMSClientsManagementTableModel() {
        clients = BSMSClientsTools.getClients();
    }

    @Override
    public int getRowCount() {
        if (clients == null) {
            return 0;
        } else {
            return clients.size();
        }
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "First Name";
            case 1:
                return "Last Name";
            case 2:
                return "Phone 1";
            case 3:
                return "Phone 2";
            default:
                return "";
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return true;
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return clients.get(row).getFname();
            case 1:
                return clients.get(row).getLname();
            case 2:
                return clients.get(row).getPhone1();
            case 3:
                return clients.get(row).getPhone2();
            default:
                return "";
        }
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        String newValue = (String) aValue;
        switch (column) {
            case 0:
                if (!newValue.equalsIgnoreCase("")) {
                    clients.get(row).setFname(newValue);
                }
                break;
            case 1:
                if (!newValue.equalsIgnoreCase("")) {
                    clients.get(row).setLname(newValue);
                }
                break;
            case 2:
                if (!newValue.equalsIgnoreCase("")) {
                    clients.get(row).setPhone1(newValue);
                }
                break;
            case 3:
                clients.get(row).setPhone2(newValue);
                break;
        }
        BSMSClientsTools.updateClient(clients.get(row));
        fireTableRowsUpdated(row, row);
    }

    @Override
    public void addRow(Object[] rowData) {
        clients.add(new BSMSClient(String.valueOf(rowData[0]),
                String.valueOf(rowData[1]),
                String.valueOf(rowData[2]),
                String.valueOf(rowData[3])));
        fireTableRowsInserted(0, clients.size());
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return clients.get(0).getFname().getClass();
            case 1:
                return clients.get(0).getLname().getClass();
            case 2:
                return clients.get(0).getPhone1().getClass();
            case 3:
                return clients.get(0).getPhone2().getClass();
            default:
                return null;
        }
    }
}
