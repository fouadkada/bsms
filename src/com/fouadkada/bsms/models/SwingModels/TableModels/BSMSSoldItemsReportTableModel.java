/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models.SwingModels.TableModels;

import com.fouadkada.bsms.controllers.Tools;
import com.fouadkada.bsms.models.BSMSSellItemDataBundle;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author fouad
 */
public class BSMSSoldItemsReportTableModel extends DefaultTableModel {

    private ArrayList<BSMSSellItemDataBundle> entries;

    public BSMSSoldItemsReportTableModel(ArrayList<BSMSSellItemDataBundle> entries) {
        if (entries != null) {
            this.entries = entries;
        }
    }

    @Override
    public void removeRow(int row) {
        entries.remove(row);
        fireTableRowsDeleted(row, row);
    }

    public void empty() {
        this.entries = null;
        fireTableRowsDeleted(0, 0);
    }

    @Override
    public int getRowCount() {
        if (this.entries == null) {
            return 0;
        } else {
            return entries.size();
        }
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Description";
            case 1:
                return "Price";
            case 2:
                return "Quantity";
            case 3:
                return "Date";
            default:
                return "";
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return entries.get(row).getDescription();
            case 1:
                return entries.get(row).getAmount();
            case 2:
                return entries.get(row).getQuantity();
            case 3:
                return Tools.formatDate(null, new Date(entries.get(row).getTimeStamp().getTime()));
            default:
                return "";
        }
    }
}
