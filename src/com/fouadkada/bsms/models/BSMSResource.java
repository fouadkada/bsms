/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models;

import com.fouadkada.bsms.controllers.BSMSSettingsTools;
import com.thirdnf.resourceScheduler.Availability;
import com.thirdnf.resourceScheduler.Resource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.joda.time.Duration;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

/**
 *
 * @author fouad
 */
public class BSMSResource implements Resource {

    private int rid;
    private String fname, lname, color;
    private boolean status;
    private BSMSSettings settings;
    private ArrayList<Availability> availabilities;

    public BSMSResource() {
        this.rid = -1;
        this.fname = "";
        this.lname = "";
        this.status = false;
        this.availabilities = null;
    }

    public BSMSResource(String fname, String lname) {
        this.fname = fname;
        this.lname = lname;
        settings = BSMSSettingsTools.getSettings();
        availabilities = new ArrayList<>();
    }

    @Override
    public String getTitle() {
        return getFname() + " " + getLname();
    }

    @Override
    public Iterator<Availability> getAvailability(LocalDate date) {
        Duration d = new Duration(settings.getEndTime().getTime() - settings.getStartTime().getTime());
        availabilities.add(new Availability(new LocalTime(settings.getStartTime().getTime()), d));
        return availabilities.iterator();
    }

    public ArrayList<Availability> getAvailability() {
        return availabilities;
    }

    public void setAvailability(List<Availability> availabilities) {
        if (availabilities != null) {
            this.availabilities = new ArrayList<>();
            this.availabilities.addAll(availabilities);
        }
    }

    /**
     * @return the fname
     */
    public String getFname() {
        return fname;
    }

    /**
     * @param fname the fname to set
     */
    public void setFname(String fname) {
        this.fname = fname;
    }

    /**
     * @return the lname
     */
    public String getLname() {
        return lname;
    }

    /**
     * @param lname the lname to set
     */
    public void setLname(String lname) {
        this.lname = lname;
    }

    /**
     * @return the rid
     */
    public int getRid() {
        return rid;
    }

    /**
     * @param rid the rid to set
     */
    public void setRid(int rid) {
        this.rid = rid;
    }

    /**
     * @return the status
     */
    public boolean getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return getTitle();
    }

    public String toStringRepresentation() {
        return getTitle() + "\n";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BSMSResource) {
            BSMSResource resource = (BSMSResource) obj;
            if (getRid() == resource.getRid()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.rid;
        return hash;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }
}
