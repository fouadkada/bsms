/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author fouad
 */
public class BSMSReport {

    private int appointmentID;
    private BSMSClient client;
    private BSMSResource resource;
    private Date appointmentDate;
    private String status;
    private ArrayList<BSMSService> services;

    public BSMSReport(){
        this.appointmentID = -1;
        this.client = null;
        this.resource = null;
        this.appointmentDate = null;
        this.services = null;
        status = "";
    }
    
    public BSMSReport(BSMSClient client,
            BSMSResource resource,
            Date appointmentDate,
            String status,
            ArrayList<BSMSService> services,
            int appointmentID) {
        this.client = client;
        this.resource = resource;
        this.appointmentDate = appointmentDate;
        this.status = status;
        this.services = services;
        this.appointmentID = appointmentID;
    }

    /**
     * @return the client
     */
    public BSMSClient getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(BSMSClient client) {
        this.client = client;
    }

    /**
     * @return the resource
     */
    public BSMSResource getResource() {
        return resource;
    }

    /**
     * @param resource the resource to set
     */
    public void setResource(BSMSResource resource) {
        this.resource = resource;
    }

    /**
     * @return the date
     */
    public Date getAppointmentDate() {
        return appointmentDate;
    }

    /**
     * @param appointmentDate the date to set
     */
    public void setAppointmentDate(Date appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the services
     */
    public ArrayList<BSMSService> getServices() {
        return services;
    }

    /**
     * @param services the services to set
     */
    public void setServices(ArrayList<BSMSService> services) {
        this.services = services;
    }

    /**
     * @return the appointmentID
     */
    public int getAppointmentID() {
        return appointmentID;
    }

    /**
     * @param appointmentID the appointmentID to set
     */
    public void setAppointmentID(int appointmentID) {
        this.appointmentID = appointmentID;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.appointmentID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BSMSReport other = (BSMSReport) obj;
        if (this.appointmentID != other.appointmentID) {
            return false;
        }
        return true;
    }
    
    
}
