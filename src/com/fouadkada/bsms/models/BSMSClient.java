/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models;

/**
 *
 * @author fouad kada
 */
public class BSMSClient {

    private int cid;
    private String fname, lname, phone1, phone2;

    public BSMSClient() {
        this.cid = -1;
        this.fname = "";
        this.lname = "";
        this.phone1 = "";
        this.phone2 = "";
    }

    public BSMSClient(String fname, String lname, String phone1, String phone2) {
        this.fname = fname;
        this.lname = lname;
        this.phone1 = phone1;
        this.phone2 = phone2;
    }

    /**
     * @return the cid
     */
    public int getCid() {
        return cid;
    }

    /**
     * @param cid the cid to set
     */
    public void setCid(int cid) {
        this.cid = cid;
    }

    /**
     * @return the fname
     */
    public String getFname() {
        return fname;
    }

    /**
     * @param fname the fname to set
     */
    public void setFname(String fname) {
        this.fname = fname;
    }

    /**
     * @return the lname
     */
    public String getLname() {
        return lname;
    }

    /**
     * @param lname the lname to set
     */
    public void setLname(String lname) {
        this.lname = lname;
    }

    /**
     * @return the phone1
     */
    public String getPhone1() {
        return phone1;
    }

    /**
     * @param phone1 the phone1 to set
     */
    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    /**
     * @return the phone2
     */
    public String getPhone2() {
        return phone2;
    }

    /**
     * @param phone2 the phone2 to set
     */
    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    @Override
    public String toString() {
        return getFname() + " " + getLname();
    }

    public String toStringRepresentation() {
        return getFname() + " " + getLname() + "\n"
                + "Phone 1: " + getPhone1() + "\n"
                + "Phone 2: " + getPhone2() + "\n";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BSMSClient other = (BSMSClient) obj;
        if (this.cid != other.cid) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.cid;
        return hash;
    }
}
