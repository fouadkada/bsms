/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.models;

import com.fouadkada.bsms.controllers.BSMSAccountingPaymentCombination;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author fouad
 */
public class BSMSAccountingReport extends BSMSReport {

    private double amountBeforeDiscount, amountAfterDiscount, discountAmount, amountPaidSoFar, remainingAmount;
    private ArrayList<BSMSAccountingPaymentCombination> combinations;

    public BSMSAccountingReport() {
        super();
        amountBeforeDiscount = 0;
        amountAfterDiscount = -1;
        combinations = null;
    }

    public BSMSAccountingReport(BSMSClient client,
            BSMSResource resource,
            Date appointmentDate,
            String status,
            ArrayList<BSMSService> services,
            int appointmentID,
            Date lastPaidDate,
            double amountBeforeDiscount,
            double amountAfterDiscount,
            ArrayList<BSMSAccountingPaymentCombination> combinations) {
        super(client, resource, appointmentDate, status, services, appointmentID);
        this.amountBeforeDiscount = amountBeforeDiscount;
        this.amountAfterDiscount = amountAfterDiscount;
        this.combinations = combinations;
    }

    /**
     * @return the amountBeforeDiscount
     */
    public double getAmountBeforeDiscount() {
        return amountBeforeDiscount;
    }

    /**
     * @param amountBeforeDiscount the amountBeforeDiscount to set
     */
    public void setAmountBeforeDiscount(double amountBeforeDiscount) {
        this.amountBeforeDiscount = amountBeforeDiscount;
    }

    /**
     * @return the amountAfterDiscount
     */
    public double getAmountAfterDiscount() {
        return amountAfterDiscount;
    }

    /**
     * @param amountAfterDiscount the amountAfterDiscount to set
     */
    public void setAmountAfterDiscount(double amountAfterDiscount) {
        this.amountAfterDiscount = amountAfterDiscount;
    }

    /**
     * @return the combinations
     */
    public ArrayList<BSMSAccountingPaymentCombination> getCombinations() {
        return combinations;
    }

    /**
     * @param combinations the combinations to set
     */
    public void setCombinations(ArrayList<BSMSAccountingPaymentCombination> combinations) {
        this.combinations = combinations;
    }

    /**
     * @return the discountAmount
     */
    public double getDiscountAmount() {
        return discountAmount;
    }

    /**
     * @param discountAmount the discountAmount to set
     */
    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    /**
     * @return the amountPaidSoFar
     */
    public double getAmountPaidSoFar() {
        return amountPaidSoFar;
    }

    /**
     * @param amountPaidSoFar the amountPaidSoFar to set
     */
    public void setAmountPaidSoFar(double amountPaidSoFar) {
        this.amountPaidSoFar = amountPaidSoFar;
    }

    /**
     * @return the remainingAmount
     */
    public double getRemainingAmount() {
        return remainingAmount;
    }

    /**
     * @param remainingAmount the remainingAmount to set
     */
    public void setRemainingAmount(double remainingAmount) {
        this.remainingAmount = remainingAmount;
    }
}
