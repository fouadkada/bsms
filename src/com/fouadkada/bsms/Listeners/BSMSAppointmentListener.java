/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.Listeners;

import com.fouadkada.bsms.BSMSCustomComponents.BSMSAppointmentComponent;
import com.thirdnf.resourceScheduler.Appointment;
import java.awt.event.MouseEvent;

/**
 *
 * @author fouad
 */
public interface BSMSAppointmentListener {

    public void handleClick(Appointment appointmentInterface,
            BSMSAppointmentComponent appointmentComponent,
            MouseEvent evt);
}
