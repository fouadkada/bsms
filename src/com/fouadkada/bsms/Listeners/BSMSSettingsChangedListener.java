/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fouadkada.bsms.Listeners;

import java.util.Date;
import java.util.EventListener;

/**
 *
 * @author fouad
 */
public interface BSMSSettingsChangedListener extends EventListener{
    
    public void startDateChanged(Date newDate);
    
    public void endDateChanged(Date newDate);
}
